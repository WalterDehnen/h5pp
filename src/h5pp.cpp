/*
 *  Copyright (c), 2020, Walter Dehnen <wdehnen64@gmail.com>
 *
 *  Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 *
 */
// TEST
#include <iomanip>
#include <iostream>
// TSET

#include "h5pp.hpp"

#include <limits>
#include <complex>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <exception>

#ifdef __clang__
//  switch off certain clang warning messages
#  pragma clang diagnostic ignored "-Wc++98-compat-pedantic"
#  pragma clang diagnostic ignored "-Wreserved-id-macro"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#  pragma clang diagnostic ignored "-Wpadded"
#endif

#define H5_USE_110_API 1

#include <H5Ipublic.h>
#include <H5Opublic.h>
#include <H5Lpublic.h>
#include <H5Epublic.h>
#include <H5Spublic.h>
#include <H5Dpublic.h>
#include <H5Tpublic.h>
#include <H5Ppublic.h>
#include <H5Apublic.h>
#include <H5Gpublic.h>
#include <H5Fpublic.h>

#ifdef __clang__
//  switch off more clang warning messages
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wswitch-enum"
#  pragma clang diagnostic ignored "-Wcovered-switch-default"
#endif

//
namespace {

using std::cerr;
using std::move;
using std::copy;
using std::vector;
using std::is_same;
using std::to_string;
using std::enable_if;
using std::is_integral;
using std::numeric_limits;

std::string to_string(const h5pp::size_type ndim, const h5pp::size_type*size)
{
    if(ndim==0) return "[]";
    std::string result = '[' + to_string(size[0]);
    for(h5pp::size_type i=1; i!=ndim; ++i) {
	result += ',' + to_string(size[i]);
    }
    return result += ']';
}


std::string to_string(h5pp::size_table const& table)
{
    return to_string(h5pp::size_type(table.size()), table.data());
}

}

namespace h5pp {

////////////////////////////////////////////////////////////////////////////////
//
// check that static declarations match those in HDF5's C API
//
////////////////////////////////////////////////////////////////////////////////
static_assert(is_same<hid_t,id_type>::value,
    "mismatch between h5pp::id_type and HDF5's hid_t");
static_assert(Object::invalid_id == H5I_INVALID_HID,
    "mismatch between h5pp::Object::invalid_id and HDF5's H5I_INVALID_HID");

static_assert(is_same<hsize_t,hsize_type>::value,
    "mismatch between h5pp::hsize_type and HDF5's hsize_t");
static_assert(sizeof(hsize_t)==sizeof(size_type),
    "size mismatch between h5pp::size_type and HDF5's hsize_t");
static_assert(numeric_limits<size_type>::is_signed ==
              numeric_limits<hsize_t>::is_signed,
    "signedness mismatch between h5pp::size_type and HDF5's hsize_t");

static_assert(sizeof(haddr_t)==sizeof(fileaddress_type),
    "size mismatch between h5pp::fileaddress_type and HDF5's haddr_t");
static_assert(numeric_limits<fileaddress_type>::is_signed ==
              numeric_limits<haddr_t>::is_signed,
    "signedness mismatch between h5pp::fileaddress_type and HDF5's haddr_t");

static_assert(H5S_UNLIMITED == DataSpace::unlimited,
    "DataSpace::unlimited does not match H5S_UNLIMITED");

static_assert(H5P_DEFAULT == 0,
    "PropertyList default identifier does not match H5P_DEFAULT");

}

namespace {

using namespace h5pp;

////////////////////////////////////////////////////////////////////////////////
//
//  error handling
//
// see § 9.4.6 of the user manual at
// https://support.hdfgroup.org/HDF5/doc/UG/HDF5_Users_Guide-Responsive%20HTML5/index.html
//
////////////////////////////////////////////////////////////////////////////////

//
// silence HDF5 error reporting in scope using RAII
//
// to be used on commands that due to dependence on client may cause an HDF5
// error and which is caught from the return argument of the HDF5 C function.
//

struct silenceNested
{
    const bool outermost;
    silenceNested()
      : outermost(0==refcount++)
    {
	// only outermost
	if(outermost) {
	    // remember previous error settings
	    H5Eget_auto2(H5E_DEFAULT, &func, &client_data);
	    // reset error settings to none
	    H5Eset_auto2(H5E_DEFAULT, nullptr, nullptr);
	}
    }
    ~silenceNested()
    {
	// only outermost
	if(outermost) {
	    refcount = 0u;
	    // reset error setting to remembered state
	    H5Eset_auto2(H5E_DEFAULT, func, client_data);
	}
    }
    static unsigned refcount;
    static H5E_auto2_t  func;
    static void* client_data;
};

unsigned silenceNested::refcount=0u;
H5E_auto2_t  silenceNested::func=nullptr;
void* silenceNested::client_data=nullptr;

//
// collect information from error stack, but avoid calling HDF5 functions
//
struct errorStackData
{
    struct errorInfo {
	string   func,file,desc;
	unsigned line;
	hid_t    cls,maj,min;
    };
    vector<errorInfo> table;
    errorInfo& get()
    {
	table.emplace_back();
	return table.back();
    }
    static
    herr_t collect(unsigned, const H5E_error2_t* errorDesc, void*clientData)
    {
	auto&info = static_cast<errorStackData*>(clientData)->get();
	info.cls  = errorDesc->cls_id;
	info.maj  = errorDesc->maj_num;
	info.min  = errorDesc->min_num;
	info.func = errorDesc->func_name? errorDesc->func_name:"";
	info.file = errorDesc->file_name? errorDesc->file_name:"";
	info.desc = errorDesc->desc? errorDesc->desc:"";
	info.line = errorDesc->line;
	return 0;
    }
};

//
// retrieve the HDF5 error stack
//
string errorStack()
{
    constexpr size_t maxMsg = 64;
    auto error_stack = H5Eget_current_stack();
    if(error_stack <= 0)
	return "";
    errorStackData errors;
    H5Ewalk2(error_stack, H5E_WALK_DOWNWARD, errorStackData::collect, &errors);
    string errorMessage;
    char   msg[maxMsg];
    for(size_t i=0; i!=errors.table.size(); ++i) {
	const auto&info = errors.table[i];
	errorMessage+= (errorMessage.empty()?"  ":"\n  ") + to_string(i) + "# ";
	// error class
	auto size = H5Eget_class_name(info.cls, msg, maxMsg);
	if(size>0 && msg[0]) {
	    errorMessage += ' ';
	    errorMessage += msg;
	}
	errorMessage += " error:";
	// function where error occured
	bool in = false;
	if(!info.func.empty()) {
	    errorMessage += " in " + info.func + "()";
	    in =  true;
	}
	// line and file where error occured
	if(!info.file.empty()) {
	    errorMessage += (in? " (file '":" in file") + info.file + '\'';
	    if(info.line > 0)
		errorMessage += " line " + to_string(info.line);
	    if(in)
		errorMessage += ')';
	}
	// description of error
	if(!info.desc.empty()) {
	    errorMessage += ": ";
	    errorMessage += info.desc;
	}
	// major error class
	bool majmin = false;
	size = H5Eget_msg(info.maj, nullptr, msg, maxMsg);
	if(size>0 && msg[0]) {
	    errorMessage += " (major: '";
	    errorMessage += msg;
	    errorMessage += '\'';
	    majmin = true;
	}
	// minor error class
	size = H5Eget_msg(info.min, nullptr, msg, maxMsg);
	if(size>0 && msg[0]) {
	    errorMessage += (majmin? ", minor: '":" (minor: '");
	    errorMessage += msg;
	    errorMessage += '\'';
	    majmin = true;
	}
	if(majmin)
	    errorMessage += ')';
    }
    return errorMessage;
}

//
// combine error message with error stack
//

static unsigned errorDepth = 1;
static unsigned debugDepth = 0;

inline string errorMessage(string const&prefixMessage)
{
    if(errorDepth) {
	auto stack = errorStack();
	if(!stack.empty()) {
	    return "h5pp: " + prefixMessage + '\n' + stack;
	}
    }
    return "h5pp: " + prefixMessage;
}

//
// throw exception with error message and error stack
//
inline void throwException [[noreturn]] (string const&prefixMessage)
{
    auto stack = errorStack();
    if(stack.empty()) {
	throw Exception{prefixMessage};
    } else {
	throw Exception{prefixMessage + '\n' + stack};
    }
}

//
// throw exception or report error, but only if no uncaught exception in flight 
//
inline void reportError(string const&prefixMessage, bool nothrow=false)
{
    if(!std::uncaught_exception()) {
	if(nothrow) {
	    cerr << (errorMessage(prefixMessage)+'\n');
	} else {
	    throwException(prefixMessage);
	}
    }
}

//
// write a debug report to stderr if level < debugDepth
//
inline void debugReport(unsigned level, string const&message)
{
    if(level < debugDepth) {
	cerr << "h5pp debug message: " << message << '\n';
    }
}

//
inline string to_string(const void*ptr)
{
    std::ostringstream ost;
    ost << ptr;
    return ost.str();
}

/// is an handle valid?
inline bool is_valid(hid_t id)
{
    if(id == H5I_INVALID_HID)
	return false;
    const auto ans = H5Iis_valid(id);
    if( ans < 0 ) {
	reportError("unable to establish validity of HDF5 handle");
    }
    return ans > 0;
}

/// auxiliary for obtaining a name from a HDF5 get_name function

//  It is most annoying that the signatures for H5Aget_name() and H5Fget_name()
//  are different. As a consequence, we cannot use the same get_name() function
//  for both, but must map one to the other (via a lambda, which requires this
//  template to take both a function pointer and a lambda)

//  signature of H5Aget_name()
typedef ssize_t (*get_string1_t)(hid_t, size_t, char*);
//  signature of H5Fget_name()
typedef ssize_t (*get_string2_t)(hid_t, char*, size_t);

template<typename GetString2>
inline string get_name(GetString2 const&get_string2, hid_t id)
{
    // 0 deal with invalid object
    if(!is_valid(id))
	return "";
    // 1 attempt reading up to 255-character name to stack
    constexpr size_type max_size=256;
    char buffer1[max_size];
    silenceNested silent;
    const auto length1 = get_string2(id, buffer1, hsize_t(max_size));
    if( length1 < 0 ) {
	reportError("failed to obtain length of object name");
    }
    if( size_type(length1) < max_size ) {
	return {buffer1};
    }
    // 2 for longer names: allocate memory on the heap
    vector<char> buffer2(size_type(length1) + 1, 0);
    const auto length2 = get_string2(id, buffer2.data(), hsize_t(max_size));
    if( length2 < 0 ) {
	reportError("failed to obtain object name");
    }
    if( length2 != length1 ) {
	throw Exception("failed to obtain object name (different lengths "
	    "for the name returned by subsequent calls to HDF5)");
    }
    return {buffer2.data(), size_type(length2)};
}

inline string get_name(get_string1_t get_string1, hid_t id)
{
    return get_name([&](hid_t i, char*b, size_t s) -> ssize_t
		    { return get_string1(i,s,b); }, id);
}

} // namespace {
//
namespace h5pp {

void setErrorReportingDepth(unsigned depth)
{
    errorDepth = depth;
}

//
void setDebugReportingDepth(unsigned depth)
{
    debugDepth = depth;
}

////////////////////////////////////////////////////////////////////////////////
//
// Exception
//
////////////////////////////////////////////////////////////////////////////////

// this is not inline so that the associated v-table is placed here
void Exception::dummy() const {}

////////////////////////////////////////////////////////////////////////////////
//
// Object
//
////////////////////////////////////////////////////////////////////////////////

bool Object::valid() const noexcept
{
    return is_valid(getId());
}

//
void Object::reset_identifier(id_type new_identifier, bool nothrow)
{
    debugReport(8,"Object::reset id from " + to_string(getId()) + " to " +
	to_string(new_identifier)  + " this=" + to_string(this));
    if(is_valid(getId()) && getId() != new_identifier) {
	silenceNested silent;
	if( H5Idec_ref(identifier) < 0) {
	    reportError("decreasing HDF5 reference counter failed", nothrow);
	}
    }
    identifier = new_identifier;
}

//
Object::Type details::type(id_type loc_id) noexcept
{
    const auto h5type = H5Iget_type(loc_id);
    switch(h5type) {
    default:            return Object::Type::Unknown;
    case H5I_BADID:     return Object::Type::Invalid;
    case H5I_FILE:      return Object::Type::File;
    case H5I_GROUP:     return Object::Type::Group;
    case H5I_DATATYPE:  return Object::Type::UserDataType;
    case H5I_DATASPACE: return Object::Type::DataSpace;
    case H5I_DATASET:  	return Object::Type::DataSet;
    case H5I_ATTR:      return Object::Type::Attribute;
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// DataType
//
////////////////////////////////////////////////////////////////////////////////

bool DataType::operator==(DataType const&other) const
{
    silenceNested silent;
    const auto ans = H5Tequal(getId(), other.getId());
    if( ans < 0 ) {
	reportError("data type comparison failed");
    }
    return ans > 0;
	
}

//
string DataType::name(Class cl) noexcept
{
    switch(cl) {
    default:               return "invalid";
    case Class::Time:      return "time";
    case Class::Integer:   return "int";
    case Class::Float:     return "float";
    case Class::String:    return "string";
    case Class::BitField:  return "bitfield";
    case Class::Opaque:    return "opaque";
    case Class::Compound:  return "compound";
    case Class::Reference: return "reference";
    case Class::Enum:      return "enum";
    case Class::VarLen:    return "varlen";
    case Class::Array:     return "array";
    }
}

//
DataType::Class DataType::getClass() const noexcept
{
    switch(H5Tget_class(getId())) {
    case H5T_TIME:      return Class::Time;
    case H5T_INTEGER:   return Class::Integer;
    case H5T_FLOAT:     return Class::Float;
    case H5T_STRING:    return Class::String;
    case H5T_BITFIELD:  return Class::BitField;
    case H5T_OPAQUE:    return Class::Opaque;
    case H5T_COMPOUND:  return Class::Compound;
    case H5T_REFERENCE: return Class::Reference;
    case H5T_ENUM:      return Class::Enum;
    case H5T_VLEN:  	return Class::VarLen;
    case H5T_ARRAY: 	return Class::Array;
    case H5T_NO_CLASS:
    case H5T_NCLASSES:
    default:            return Class::Invalid;
    }
}

//
size_type DataType::size() const
{
    const auto ans = H5Tget_size(getId());
    if( ans <= 0 ) {
	reportError("unable to obtain size of data type");
    }
    return size_type(ans);
}

//
bool DataType::isSigned() const noexcept
{
    return
	H5Tget_class(getId()) == H5T_INTEGER &&
	H5Tget_sign (getId()) == H5T_SGN_2;
}

//
bool DataType::isUnsigned() const noexcept
{
    return
	H5Tget_class(getId()) == H5T_INTEGER &&
	H5Tget_sign (getId()) == H5T_SGN_NONE;
}

//
string DataType::description() const
{
    auto clss = getClass();
    auto desc = name(clss);
    switch(clss) {
    default: break;
    case Class::Integer:
	if( H5Tget_sign(getId())==H5T_SGN_NONE )
	    desc = 'u' + desc;
#ifdef __clang__
	[[clang::fallthrough]];
#elif defined(__GNUC__) && __GNUC__ > 6
	__attribute__ ((fallthrough));
#endif
    case Class::Enum:
    case Class::Float:
	desc += to_string(8*size());
    }
    return desc;
}

//
id_type DataType::copy(id_type type_id)
{
    silenceNested silent;
    const auto copy_id = H5Tcopy(type_id);
    if( copy_id < 0 ) {
	reportError("unable to copy data type");
    }
    debugReport(8,"DataType::copy: id " + to_string(type_id) + " -> " +
	to_string(copy_id));
    return copy_id;
}
} // namespace h5pp
//
namespace {

template<typename> struct DataTypeTraits;

template<> struct DataTypeTraits<bool>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_HBOOL); } };

template<> struct DataTypeTraits<char>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_CHAR); } };

template<> struct DataTypeTraits<signed char>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_CHAR); } };

template<> struct DataTypeTraits<unsigned char>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_UCHAR); } };

template<> struct DataTypeTraits<short>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_SHORT); } };

template<> struct DataTypeTraits<unsigned short>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_USHORT); } };

template<> struct DataTypeTraits<int>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_INT); } };

template<> struct DataTypeTraits<unsigned int>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_UINT); } };

template<> struct DataTypeTraits<long>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_LONG); } };

template<> struct DataTypeTraits<unsigned long>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_ULONG); } };

template<> struct DataTypeTraits<long long>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_LLONG); } };

template<> struct DataTypeTraits<unsigned long long>
{ static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_ULLONG); } };

template<> struct DataTypeTraits<float>
{
    static hid_t native() noexcept { return H5T_NATIVE_FLOAT; }
    static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_FLOAT); }
};

template<> struct DataTypeTraits<double>
{
    static hid_t native() noexcept { return H5T_NATIVE_DOUBLE; }
    static hid_t makeId(bool) noexcept { return H5Tcopy(H5T_NATIVE_DOUBLE); }
};

//
template<> struct DataTypeTraits<string>
{
    static hid_t makeId(bool utf8)
    {
	silenceNested silent;
	auto id = H5Tcopy(H5T_C_S1);
	if( H5Tset_size(id, H5T_VARIABLE) < 0 ) {
	    reportError("unable to set string datatype to variable-length");
	}
	if( H5Tset_cset(id, utf8? H5T_CSET_UTF8 : H5T_CSET_ASCII) < 0 ) {
	    reportError("unable to set encoding for string datatype");
	}
	return id;
    }
};

//
template<typename Float>
struct DataTypeTraits<std::complex<Float> >
{
    static hid_t makeId(bool)
    {
	auto id = H5Tcreate(H5T_COMPOUND, sizeof(std::complex<Float>));
	H5Tinsert(id, "r", 0, DataTypeTraits<Float>::native());
	H5Tinsert(id, "i", sizeof(Float), DataTypeTraits<Float>::native());
	return id;
    }
};

} // namespace {
//
namespace h5pp {

template<typename T>
DataType DataType::typeFor(bool u) noexcept(is_basic_type<T>())
{ return DataType(DataTypeTraits<T>::makeId(u)); }

#define DefineDataTypeFor(TYPE)			    \
    template DataType DataType::typeFor<TYPE>(bool) \
      noexcept(is_basic_type<TYPE>());

DefineDataTypeFor(bool);
DefineDataTypeFor(char);
DefineDataTypeFor(signed char);
DefineDataTypeFor(unsigned char);
DefineDataTypeFor(short);
DefineDataTypeFor(unsigned short);
DefineDataTypeFor(int);
DefineDataTypeFor(unsigned int);
DefineDataTypeFor(long);
DefineDataTypeFor(unsigned long);
DefineDataTypeFor(long long);
DefineDataTypeFor(unsigned long long);
DefineDataTypeFor(float);
DefineDataTypeFor(double);
DefineDataTypeFor(std::complex<float>);
DefineDataTypeFor(std::complex<double>);
DefineDataTypeFor(string);

#undef DefineDataTypeFor

//
bool isVariableLengthString(DataType const&dtype)
{
    const auto res = H5Tis_variable_str(dtype.getId());
    if( res < 0) {
	reportError("failed to enquire whether data type "
	    "is variable-length string");
    }
    return res;
}

//
bool isUTF8EncodedString(DataType const&dtype)
{
    return
	H5Tget_class(dtype.getId()) == H5T_STRING &&
	H5Tget_cset(dtype.getId()) == H5T_CSET_UTF8;
}

//
bool isASCIIEncodedString(DataType const&dtype)
{
    return
	H5Tget_class(dtype.getId()) == H5T_STRING &&
	H5Tget_cset(dtype.getId()) == H5T_CSET_ASCII;
}

} // namespace h5pp
////////////////////////////////////////////////////////////////////////////////
//
// DataSpace
//
////////////////////////////////////////////////////////////////////////////////
namespace {

template<typename SizeType>
inline SizeType product(vector<SizeType> const&extent)
{
    return std::accumulate(extent.begin(),extent.end(),SizeType(1),
	std::multiplies<SizeType>());
}

//
bool have_identical_extent(id_type left, id_type rght)
{
    silenceNested silent;
    const auto ndimLeft = H5Sget_simple_extent_ndims(left);
    if( ndimLeft < 0 ) {
	reportError("unable to get number of data space dimensions");
    }
    const auto ndimRght = H5Sget_simple_extent_ndims(rght);
    if( ndimRght < 0 ) {
	reportError("unable to get number of data space dimensions");
    }
    if(ndimLeft != ndimRght) {
	return false;
    }
    switch(const auto ndim = size_type(ndimLeft)) {
    case 0:
	return true;
    case 1: case 2: {
	hsize_t dims[4];
	if( H5Sget_simple_extent_dims(left, dims, nullptr) < 0 ) {
	    reportError("unable to get extent of data space");
	}
	if( H5Sget_simple_extent_dims(rght, dims+2, nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
	for(size_type d=0; d!=ndim; ++d) {
	    if(dims[d]!=dims[d+2]) {
		return false;
	    }
	}
	return true;
    }
    default: {
	vector<hsize_t> dims(ndim+ndim);
 	if( H5Sget_simple_extent_dims(left, dims.data(), nullptr) < 0 ) {
	    reportError("unable to get extent of data space");
	}
 	if( H5Sget_simple_extent_dims(rght, dims.data()+ndim, nullptr) < 0 ) {
	    reportError("unable to get extent of data space");
	}
	for(size_type d=0; d!=ndim; ++d) {
	    if(dims[d]!=dims[ndim+d]) {
		return false;
	    }
	}
	return true;
    }
    }
}

//
template<typename SizeType>
inline size_type number_selected(vector<SizeType> const&extnt,
    size_table const&start, size_table const&count, size_table const&strde)
  noexcept
{
    size_type count_selected = 1;
    for(size_type dim=0; dim!=extnt.size() && count_selected; ++dim) {
	count_selected *=
	    details::selection_count(extnt[dim],
		details::get_value(dim, start, 0),
		details::get_value(dim, count, size_type(extnt[dim])),
		details::get_value(dim, strde, 1));
    }
    return count_selected;
}

} // namespace{
//
namespace h5pp {

bool haveIdenticalExtent(DataSpace const&a, DataSpace const&b)
{
    return have_identical_extent(a.getId(), b.getId());
}

// tested 22-01-2020
id_type DataSpace::copy(id_type space_id)
{
    if(space_id == H5S_ALL) {
	return H5S_ALL;
    }
    silenceNested silent;
    const auto copy_id = H5Scopy(space_id);
    if( copy_id < 0 ) {
	reportError("unable to copy data space");
    }
    debugReport(8,"DataSpace::copy: id " + to_string(space_id) + " -> " +
	to_string(copy_id));
    return copy_id;
}

//
id_type DataSpace::make_scalar() noexcept
{
    auto id = H5Screate(H5S_SCALAR);
    debugReport(8,"DataSpace::make_scalar(): id=" + to_string(id));
    return id;
}

// tested 21-02-2020
bool DataSpace::isScalar() const
{
    silenceNested silent;
    const auto val = H5Sget_simple_extent_type(getId());
    if( val < 0 ) {
	reportError("unable to infer whether data space is scalar");
    }
    return val == H5S_SCALAR;
}

//
id_type DataSpace::make_null() noexcept
{
    return H5Screate(H5S_NULL);
}

//
id_type DataSpace::make_simple(
    const size_type ndim, const size_type*iniExtent,
    const size_type mdim, const size_type*maxExtent, Extend extendible)
{
    id_type id = Object::invalid_id;
    silenceNested silent;
    debugReport(6,"DataSpace::make_simple: iniExtent=" +
	to_string(ndim,iniExtent) + ", maxExtent=" + to_string(mdim,maxExtent));
    if(ndim==0) {
	id = H5Screate(H5S_NULL);
    } else if(ndim==1) {
	hsize_t iExt = iniExtent[0];
	if(mdim || extendible!=Extend::Not) {
	    hsize_t mExt = mdim? maxExtent[0] : H5S_UNLIMITED;
	    id = H5Screate_simple(1, &iExt, &mExt);
	} else {
	    id = H5Screate_simple(1, &iExt, nullptr);
	}
    } else {
	const auto needMaxExt = mdim>0 || extendible!=Extend::Not;
	vector<hsize_t> table(ndim +(needMaxExt ? ndim : 0));
	auto iExtent = table.data();
	auto mExtent = needMaxExt? iExtent+ndim : nullptr;
	for(size_type i=0; i!=ndim; ++i) {
	    iExtent[i] = iniExtent[i];
	    if(mdim>0) {
		mExtent[i] = i<mdim? maxExtent[i] : iniExtent[i];
	    } else if(extendible==Extend::FirstDim) {
		mExtent[i] = i? iExtent[i] : H5S_UNLIMITED;
	    } else if(extendible==Extend::AllDims) {
		mExtent[i] = H5S_UNLIMITED;
	    }
	}
	id = H5Screate_simple(int(ndim), iExtent, mExtent);
    }
    if(id < 0) {
	reportError("DataSpace::make_simple(): "
	    "couldn't create data space with rank " + to_string(ndim));
    }
    debugReport(8,"DataSpace::make_simple(): id = " + to_string(id) +
	" rank=" + to_string(ndim));
    return id;
}

// tested 22-01-2020
size_type DataSpace::totalcount(id_type space_id)
{
    silenceNested silent;
    const auto ndim = H5Sget_simple_extent_ndims(space_id);
    if(ndim < 0) {
	reportError("unable to get number of data space dimensions");
    }
    switch(ndim) {
    case 0:
	return 1;
    case 1: {
	hsize_t size;
	if( H5Sget_simple_extent_dims(space_id, &size, nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
	return size_type(size);
    }
    case 2: case 3: {
	hsize_t dims[3]={1,1,1};
	if( H5Sget_simple_extent_dims(space_id, dims, nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
	return dims[0]*dims[1]*dims[2];
    }
    default:
	vector<hsize_t> dims{size_t(ndim)};
 	if( H5Sget_simple_extent_dims(space_id, dims.data(), nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
	return size_type(product(dims));
    }
}

// tested 21-02-2020
size_type DataSpace::numDimensions() const
{
    silenceNested silent;
    const auto ndim = H5Sget_simple_extent_ndims(getId());
    if(ndim < 0) {
	reportError("unable to get number of data space dimensions");
    }
    return size_type(ndim);
}

// debugged 22-01-2020
size_table DataSpace::extent() const
{
    silenceNested silent;
    size_table dims;
    vector<hsize_t> hdims(rank());
    if( hdims.size() > 0 ) {
	if( H5Sget_simple_extent_dims(getId(), hdims.data(), nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
	dims.resize(hdims.size());
	std::copy(hdims.begin(), hdims.end(), dims.begin());
    }
    return dims;
}

//
size_table DataSpace::maxExtent() const
{
    silenceNested silent;
    size_table dims;
    vector<hsize_t> hdims(rank());
    if( hdims.size() > 0 ) {
	if( H5Sget_simple_extent_dims(getId(), nullptr, hdims.data()) < 0) {
	    reportError("unable to get maximum extent of data space");
	}
	dims.resize(hdims.size());
	std::copy(hdims.begin(), hdims.end(), dims.begin());
    }
    return dims;
}

//
void DataSpace::getExtent(size_table&curExtent, size_table&maxExtent) const
{
    silenceNested silent;
    const auto ndim = rank();
    if(ndim) {
	vector<hsize_t> curdims(ndim), maxdims(ndim);
	if( H5Sget_simple_extent_dims(getId(), curdims.data(),
		maxdims.data()) < 0) {
	    reportError("unable to enquire extent of data space");
	}
	curExtent.resize(ndim);
	std::copy(curdims.begin(), curdims.end(), curExtent.begin());
	maxExtent.resize(ndim);
	std::copy(maxdims.begin(), maxdims.end(), maxExtent.begin());
    } else {
	curExtent.clear();
	maxExtent.clear();
    }
}

//
bool DataSpace::isExtendible() const
{
    silenceNested silent;
    const auto ndim = rank();
    if( ndim==0 ) {
	return false;
    }
    vector<hsize_t> dims(2*ndim);
    if( H5Sget_simple_extent_dims(getId(), dims.data(), dims.data()+ndim) < 0) {
	reportError("unable to get maximum extent of data space");
    }
    for(size_type dim=0; dim!=ndim; ++dim) {
	if(dims[dim] < dims[ndim+dim]) {
	    return true;
	}
    }
    return false;
}

//
void DataSpace::select(Selection const&S)
{
    // obtain extent
    silenceNested silent;
    size_type ndim = rank();
    vector<hsize_t> hextnt(ndim);
    if( ndim > 0 ) {
	if( H5Sget_simple_extent_dims(getId(), hextnt.data(), nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
    }
    // deal with trivial selector
    if(S.trivial()) {
	if( H5Sselect_all(getId()) < 0) {
	    reportError("unable to remove selection from data space");
	}
	num_selected = size_type(product(hextnt));
	return;
    }
    // construct hsize_t tables for start, count, and stride
    // accumulate products of extent and counts
    vector<hsize_t> hstart(ndim), hcount(ndim), hstrde(S.stride.empty()?0:ndim);
    size_type product_count = 1;
    size_type product_extnt = 1;
    for(size_type dim=0; dim!=ndim; ++dim) {
	auto extnt = size_type(hextnt[dim]);
	auto start = details::get_value(dim,S.start,0);
	auto strde = details::get_value(dim,S.stride,1);
	auto count = details::get_value(dim,S.count,extnt);
	count = details::selection_count(extnt,start,count,strde);
	product_count *= count;
	product_extnt *= extnt;
	hstart[dim] = hsize_t(start);
	hcount[dim] = hsize_t(count);
	if( hstrde.size() ) {
	    hstrde[dim] = hsize_t(strde);
	}
    }
    assert(product_count <= product_extnt);
    // apply selection
    if((product_count == 0 ?              H5Sselect_none(getId()) :
	product_count == product_extnt ?  H5Sselect_all (getId()) :
	H5Sselect_hyperslab(getId(), H5S_SELECT_SET, hstart.data(),
	    hstrde.data(), hcount.data(), nullptr)) < 0 ) {
	reportError("unable to select from data space");
    }
    num_selected = product_count;
}

//
size_type DataSpace::Selection::numSelected(size_table const&extent)
  const noexcept
{
    return number_selected(extent,start,count,stride);
}

//
size_type DataSpace::Selection::numSelected(DataSpace const&space) const
{
    silenceNested   silent;
    vector<hsize_t> extent(space.rank());
    if( extent.size() > 0 ) {
	if( H5Sget_simple_extent_dims(space.getId(), extent.data(),
		nullptr) < 0) {
	    reportError("unable to get extent of data space");
	}
    }
    return number_selected(extent,start,count,stride);
}

// tested 22-01-2020
void DataSpace::resetSelection()
{
    silenceNested silent;
    if( H5Sselect_all(getId()) < 0) {
	reportError("unable to remove selection from data space");
    }
    num_selected = totalcount(getId());
}

// tested 22-01-2020
bool DataSpace::hasSelection() const
{
    silenceNested silent;
    const auto sel = H5Sget_select_type(getId());
    if( sel < 0 ) {
	reportError("unable to establish selection type");
    }
    return sel == H5S_SEL_HYPERSLABS;
}

} // namespace h5pp

////////////////////////////////////////////////////////////////////////////////
//
// PropertyList
//
////////////////////////////////////////////////////////////////////////////////
namespace {
inline hid_t convert_plist_type(propertyType type)
{
    switch(type) {
    case propertyType::fileAccess:
	return H5P_FILE_ACCESS;
    case propertyType::datasetCreate:
	return H5P_DATASET_CREATE;
    case propertyType::datasetAccess:
	return H5P_DATASET_ACCESS;
    // case propertyType::objectCreate:
    // 	return H5P_OBJECT_CREATE;
    // case propertyType::fileCreate:
    // 	return H5P_FILE_CREATE;
    // case propertyType::dataTransfer:
    // 	return H5P_DATASET_XFER;
    // case propertyType::groupAccess:
    // 	return H5P_GROUP_ACCESS;
    // case propertyType::datatypeCreate:
    // 	return H5P_DATATYPE_CREATE;
    // case propertyType::datatypeAccess:
    // 	return H5P_DATATYPE_ACCESS;
    // case propertyType::stringCreate:
    // 	return H5P_STRING_CREATE;
    // case propertyType::attributeCreate:
    // 	return H5P_ATTRIBUTE_CREATE;
    // case propertyType::objectCopy:
    // 	return H5P_OBJECT_COPY;
    // case propertyType::linkCreate:
    // 	return H5P_LINK_CREATE;
    // case propertyType::linkAccess:
    // 	return H5P_LINK_ACCESS;
    default:
	throw Exception("unsupported propertyType: "+to_string(int(type)));
    }
}

} // namespace {
//
namespace h5pp {

string name(propertyType type)
{
    switch(type) {
    case propertyType::fileAccess:
	return "fileAccess";
    case propertyType::datasetCreate:
	return "datasetCreate";
    case propertyType::datasetAccess:
	return "datasetAccess";
    // case propertyType::objectCreate:
    // 	return "objectCreate";
    // case propertyType::fileCreate:
    // 	return "fileCreate";
    // case propertyType::dataTransfer:
    // 	return "dataTransfer";
    // case propertyType::groupAccess:
    // 	return "groupAccess";
    // case propertyType::datatypeCreate:
    // 	return "datatypeCreate";
    // case propertyType::datatypeAccess:
    // 	return "datatypeAccess";
    // case propertyType::stringCreate:
    // 	return "stringCreate:";
    // case propertyType::attributeCreate:
    // 	return "attributeCreate";
    // case propertyType::objectCopy:
    // 	return "objectCopy";
    // case propertyType::linkCreate:
    // 	return "linkCreate";
    // case propertyType::linkAccess:
    // 	return "linkAccess";
    default:
	throw Exception("unsupported propertyType: "+to_string(int(type)));
    }
}

//
string propertiesName(propertyType type)
{
    switch(type) {
    case propertyType::fileAccess:
	return "FileAccessProperties";
    case propertyType::datasetCreate:
	return "DataSetCreateProperties";
    case propertyType::datasetAccess:
	return "DataSetAccessProperties";
    default:
	return "PropertyList<"+name(type)+'>';
    }
}

//
template <propertyType T>
PropertyList<T>::PropertyList()
  : identifier(H5P_DEFAULT)
{
    debugReport(8,propertiesName(T) + "::" + propertiesName(T) +
	": default constructed " + " id=" + to_string(identifier) +
	" this=" + to_string(this));
}

//
template <propertyType T>
PropertyList<T>::PropertyList(id_type id)
  : identifier(id)
{
    debugReport(8,propertiesName(T) + "::" + propertiesName(T) +
	": constructed from id=" + to_string(identifier) + " this=" +
	to_string(this));
}

//
template <propertyType T>
PropertyList<T>::PropertyList(PropertyList const& other)
  : identifier(H5P_DEFAULT)
{
    if((identifier = H5Pcopy(other.identifier)) < 0) {
	reportError("unable to copy "+propertiesName(type));
    }
    debugReport(8,propertiesName(T) + "::" + propertiesName(T) +
	": copy constructed from " + to_string(&other) + " with id=" +
	to_string(other.getId()) + " id=" + to_string(identifier) +
	" this=" + to_string(this));
}

//
template <propertyType T>
PropertyList<T>::PropertyList(PropertyList&& other)
  : identifier(other.identifier)
{
    other.identifier = H5P_DEFAULT;
    debugReport(8,propertiesName(T) + "::" + propertiesName(T) +
	": move constructed from " + to_string(&other) + " id=" +
	to_string(identifier) + " this=" + to_string(this));
}

//
template <propertyType T>
PropertyList<T>& PropertyList<T>::operator=(PropertyList&& other)
{
    // see https://stackoverflow.com/a/9322542/1023390
    // for a note on self-move assignment
    if( identifier != H5P_DEFAULT && H5Pclose(identifier) < 0) {
	reportError("unable to close " + propertiesName(type));
    }
    identifier = other.identifier;
    other.identifier = H5P_DEFAULT;
    debugReport(8,propertiesName(T) + ": moved from " + to_string(&other) +
	" id=" + to_string(identifier) + " this=" + to_string(this));
    return *this;
}

//
template<propertyType T>
PropertyList<T>& PropertyList<T>::operator=(PropertyList const&other)
{
    if( identifier != other.identifier ) {
	if( identifier != H5P_DEFAULT && H5Pclose(identifier) < 0) {
	    reportError("unable to close " + propertiesName(type));
	}
	if((identifier = H5Pcopy(other.identifier)) < 0) {
	    reportError("unable to copy " + propertiesName(type));
	}
    }
    debugReport(8,propertiesName(T) + ": copied from " + to_string(&other) +
	" id=" + to_string(identifier) + " this=" + to_string(this));
    return *this;
}

//
template<propertyType T>
PropertyList<T>::~PropertyList()
{
    debugReport(8,propertiesName(T) + ": destroying: id=" +
	to_string(identifier) + "this=" + to_string(this));
    if( identifier != H5P_DEFAULT && H5Pclose(identifier) < 0) {
	reportError("unable to close "+propertiesName(type), true);
    }
}

//
template<propertyType T>
PropertyList<T>& PropertyList<T>::add(property const&prop)
{
    if( identifier == H5P_DEFAULT &&
	(identifier = H5Pcreate(convert_plist_type(T))) < 0) {
	reportError("unable to create " + propertiesName(type));
	debugReport(6,propertiesName(T) + "::add(): initialising to "
	    "non-default: id=" + to_string(identifier) + " this=" +
	    to_string(this));
    } else {
	debugReport(6,propertiesName(T) + "::add(): already initalised "
	    "(id=" + to_string(identifier) + ", this=" + to_string(this) + ')');
    }
    prop(identifier);
    return*this;
}

template struct PropertyList<propertyType::fileAccess>;
template struct PropertyList<propertyType::datasetCreate>;
template struct PropertyList<propertyType::datasetAccess>;
} // namespace h5pp
////////////////////////////////////////////////////////////////////////////////
//
// Chunked, Shuffle, CompressGzip, Fletcher32, Caching
//
////////////////////////////////////////////////////////////////////////////////
namespace {

inline bool has_chunked_layout(id_type plist, propertyType pType)
{
    if(plist == H5P_DEFAULT) {
	return false;
    }
    switch( H5Pget_layout(plist) ) {
    case H5D_COMPACT:
    case H5D_CONTIGUOUS:
	return false;
    case H5D_CHUNKED:
	return true;
    default:
	reportError("unable to query 'chunked' layout for " +
	    propertiesName(pType) + " with id=" + to_string(plist));
    }
    return false;
}

} // namespace {
//
namespace h5pp {
namespace details {

id_type default_properties_id()
{
    return H5P_DEFAULT;
}

//
template<propertyType pType>
inline id_type propertyBase::getPropId(PropertyList<pType> const&plist)
{
    return plist.identifier;
}

// this is not inline so that the associated v-table is placed here
propertyBase::~propertyBase() {}

} // namespace h5pp::details
//


//
void Chunked::apply(id_type plist_id, propertyType pType) const
{
    assert(pType == propertyType::datasetCreate);
    debugReport(6,"Chunked::apply(plist_id=" + to_string(plist_id) +
	"): chunks=" + to_string(chunks));
    silenceNested silent;
    if( plist_id == H5P_DEFAULT ) {
	throw Exception("h5pp: internal error: attempting to set "
	    "chunked layout for default property list");
    }
    if(chunks.size()==1) {
	hsize_t chunk = chunks[0];
	if( H5Pset_chunk(plist_id, 1, &chunk) < 0) {
	    reportError("unable to set chunk sizes for " +
		propertiesName(pType));
	}
    } else if(chunks.size()>1) {
	vector<hsize_t> hchunks(chunks.size());
	copy(chunks.begin(), chunks.end(), hchunks.begin());
	if( H5Pset_chunk(plist_id, int(chunks.size()), hchunks.data()) < 0) {
	    reportError("unable to set chunk sizes for " +
		propertiesName(pType));
	}
    }
    debugReport(8,"Chunked::apply(): set chunks");
}

//
void Chunked::check() const
{
    if(chunks.size() == 0)
	throw Exception("Chunked::Chunked: no chunk sizes provided");
}

//
bool Chunked::has(DataSetCreateProperties const&plist)
{
    return has_chunked_layout(getPropId(plist), propertyType::datasetCreate);
}

//
size_table Chunked::getChunks(DataSetCreateProperties const&plist)
{
    silenceNested silent;
    if(!has(plist)) {
	throw Exception("Chunked::getChunks(): DataSetCreateProperties "
	    "provide no chunked layout");
    }
    auto ndim1 = H5Pget_chunk(getPropId(plist), 0, nullptr);
    if( ndim1 < 0 ) {
	reportError("unable to query rank of chunk sizes");
    }
    vector<hsize_t> hchnk(static_cast<size_type>(ndim1));
    auto ndim2 = H5Pget_chunk(getPropId(plist), ndim1, hchnk.data());
    if( ndim2 < 0 ) {
	reportError("unable to query chunk sizes ");
    }
    if( ndim1 != ndim2 ) {
	throw Exception("subsequent H5Pget_chunk() return different results");
    }
    size_table chnks;
    copy(hchnk.begin(), hchnk.end(), chnks.begin());
    return chnks;
}

//
bool CompressGzip::available()
{
    return H5Zfilter_avail(H5Z_FILTER_DEFLATE);
}

//
void CompressGzip::apply(id_type plist, propertyType pType) const
{
    assert(pType == propertyType::datasetCreate);
    debugReport(6,"CompressGzip::apply(plist id=" + to_string(plist) + ')');
    silenceNested silent;
    if(!available()) {
	throw Exception("gzip compression not available: cannot apply it to " +
	    propertiesName(pType));
    }
    if( plist == H5P_DEFAULT ) {
	throw Exception("h5pp: internal error: attempting to set "
	    "compression for default property list");
    }
    if(!has_chunked_layout(plist, pType)) {
	throw Exception("gzip compression requires chunked layout");
    }
    if( H5Pset_deflate(plist, level) < 0 ) {
	reportError("unable to set gzip compression filter for " +
	    propertiesName(pType));
    }
}

//
bool CompressGzip::has(DataSetCreateProperties const&plist)
{
    unsigned flags;
    size_t  nval=0;
    silenceNested silent;
    auto ans = H5Pget_filter_by_id2(getPropId(plist), H5Z_FILTER_DEFLATE,
	&flags, &nval, nullptr, size_t(0), nullptr, nullptr);
    if( ans < 0 ) {
	reportError("unable to query gzip compression filter for "
	    "DataSetCreateProperties");
    }
    return ans > 0;
}

//
unsigned CompressGzip::getLevel(DataSetCreateProperties const&plist)
{
    unsigned flags;
    size_t  nval=1;
    unsigned level;
    silenceNested silent;
    auto ans = H5Pget_filter_by_id2(getPropId(plist), H5Z_FILTER_DEFLATE,
	&flags, &nval, &level, size_t(0), nullptr, nullptr);
    if( ans < 0 ) {
	reportError("unable to query gzip compression filter for "
	    "DataSetCreateProperties");
    }
    return ans==0? 0 : level;
}

//
void Fletcher32::apply(id_type plist, propertyType pType) const
{
    assert(pType == propertyType::datasetCreate);
    silenceNested silent;
    if( plist == H5P_DEFAULT ) {
	throw Exception("h5pp: internal error: attempting to set "
	    "Fletcher32 filter for default property list");
    }
    if(!has_chunked_layout(plist, pType)) {
	throw Exception("Fletcher32 filter requires chunked layout");
    }
    if(H5Pset_fletcher32(plist) < 0) {
	reportError("unable to set Fletcher32 checksum filter for " +
	    propertiesName(pType));
    }
}

//
bool Fletcher32::has(DataSetCreateProperties const&plist)
{
    unsigned flags;
    size_t  nval=0;
    silenceNested silent;
    auto ans = H5Pget_filter_by_id2(getPropId(plist), H5Z_FILTER_FLETCHER32,
	&flags, &nval, nullptr, size_t(0), nullptr, nullptr);
    if( ans < 0 ) {
	reportError("unable to query Fletcher32 checksum filter for "
	    "DataSetCreateProperties");
    }
    return ans > 0;
}

//
bool Shuffle::available()
{
    return H5Zfilter_avail(H5Z_FILTER_SHUFFLE);
}

//
bool Shuffle::has(DataSetCreateProperties const&plist)
{
    unsigned flags;
    size_t  nval=0;
    silenceNested silent;
    auto ans = H5Pget_filter_by_id2(getPropId(plist), H5Z_FILTER_SHUFFLE,
	&flags, &nval, nullptr, size_t(0), nullptr, nullptr);
    if( ans < 0 ) {
	reportError("unable to query 'shuffle' filter for " +
	    propertiesName(propertyType::datasetAccess));
    }
    return ans > 0;
}

//
void Shuffle::apply(id_type plist_id, propertyType pType) const
{
    assert(pType == propertyType::datasetCreate);
    silenceNested silent;
    if(!available()) {
	throw Exception("shuffle filter not available: cannot apply it to " +
	    propertiesName(pType));
    }
    if( plist_id == H5P_DEFAULT ) {
	throw Exception("h5pp: internal error: attempting to set "
	    "shuffle filter for default property list");
    }
    if(H5Pset_shuffle(plist_id) < 0) {
	reportError("unable to set shuffle filter for " +
	    propertiesName(pType));
    }
}

//
void Caching::apply(id_type plist_id, propertyType pType) const
{
    debugReport(6,"Caching::apply(plist=" + to_string(plist_id) +
	" [H5P_DEFAULT=" + to_string(H5P_DEFAULT) + "], pType=" +
	name(pType) + ')' );
    silenceNested silent;
    if( plist_id == H5P_DEFAULT ) {
	throw Exception("h5pp: internal error: attempting to set "
	    "cache for default property list");
    }
    switch(pType) {
    case propertyType::datasetAccess: {
	auto nslots = numSlots? numSlots : H5D_CHUNK_CACHE_NBYTES_DEFAULT;
	auto nbytes = cacheSize? cacheSize : H5D_CHUNK_CACHE_NBYTES_DEFAULT;
	auto policy = (preemptionPolicy > 0 && preemptionPolicy < 1)?
	    preemptionPolicy : double(H5D_CHUNK_CACHE_W0_DEFAULT);
	if(H5Pset_chunk_cache(plist_id, nslots, nbytes, policy) < 0) {
	    reportError("unable to set cache parameters for " +
		propertiesName(pType));
	}
    } break;
    case propertyType::fileAccess: {
	auto nslots = numSlots? numSlots : 521;
	auto nbytes = cacheSize? cacheSize : size_type(1)<<20;
	auto policy = (preemptionPolicy > 0 && preemptionPolicy < 1)?
	    preemptionPolicy : 0.75;
	if(H5Pset_cache(plist_id, 0, nslots, nbytes, policy) < 0) {
	    reportError("unable to set cache parameters for " +
		propertiesName(pType));
	}
    } break;
    default:
	throw Exception("internal h5pp error: unexpected '" +
	    propertiesName(pType) + "' for Caching");
    }
}

//
Caching Caching::get(id_type plist_id, propertyType pType)
{
    Caching C;
    silenceNested silent;
    switch(pType) {
    case propertyType::datasetAccess:
	if( H5Pget_chunk_cache(plist_id, &C.numSlots, &C.cacheSize,
		&C.preemptionPolicy) < 0 ) {
	    reportError("unable to obtain cache parameters for " +
		propertiesName(pType));
	}
	break;
    case propertyType::fileAccess:
	if( H5Pget_cache(plist_id, nullptr, &C.numSlots, &C.cacheSize,
		&C.preemptionPolicy) < 0 ) {
	    reportError("unable to obtain cache parameters for " +
		propertiesName(pType));
	}
	break;
    default:
	throw Exception("internal h5pp error: unexpected '" +
	    propertiesName(pType) + "' in Caching::get()");
    }
    return C;
}

////////////////////////////////////////////////////////////////////////////////
//
// Attribute
//
////////////////////////////////////////////////////////////////////////////////

// tested 21-01-2020
string Attribute::name() const
{
    return get_name(H5Aget_name,getId());
}

// tested 21-01-2020
size_type Attribute::storageSize() const
{
    return static_cast<size_t>(H5Aget_storage_size(getId()));
}

// tested 21-01-2020
DataType Attribute::dataType() const
{
    return {H5Aget_type(getId())};
}

//
size_type Attribute::size() const
{
    silenceNested silent;
    const auto space = H5Aget_space(getId());
    if(space < 0) {
        reportError("unable to obtain data space attribute (needed for size)");
    }
    const auto ndim = H5Sget_simple_extent_ndims(space);
    if( ndim < 0 ) {
	reportError("unable to get rank of data space (for Attribute size)");
    } else if( ndim == 0) {
	return 1;
    } else if( ndim > 1 ) {
	reportError("rank of Attribute data space is "+to_string(ndim)+" > 1");
    }
    hsize_t size;
    if( H5Sget_simple_extent_dims(space, &size, nullptr) < 0) {
	reportError("unable to get extent of attribute data space");
    }
    return size_type(size);
}

// tested 21-01-2020
void Attribute::read(void*buffer, DataType const&memType) const
{
    if(buffer==nullptr)
	throw Exception("Attribute['"+name()+"']::read(void*buffer=0x0)");
    const auto mem_type_id = is_valid(memType.getId())?
	memType.getId() : H5Aget_type(getId());
    if(H5Aread(getId(), mem_type_id, buffer) < 0) {
        reportError("reading from attribute failed");
    }
}

// tested 20-01-2020
void Attribute::write(const void*buffer, DataType const&memType)
{
    if(buffer==nullptr)
	throw Exception("Attribute['"+name()+"']::write(void*buffer=0x0)");
    const auto mem_type_id = is_valid(memType.getId())?
	memType.getId() : H5Aget_type(getId());
    silenceNested silent;
    if(H5Awrite(getId(), mem_type_id, buffer) < 0) {
        reportError("writing to attribute failed");
    }
}

// tested 21-01-2020
void Attribute::read(string&strg) const
{
    silenceNested silent;
    if( size() != 1 ) {
	throw Exception("cannot read scalar from non-scalar attribute");
    }
    const auto strtype = DataType::typeFor<string>();
    char* Cstrg;
    if( H5Aread(getId(), strtype.getId(), static_cast<void*>(&Cstrg)) < 0) {
        reportError("reading variable-length string from attribute failed");
    }
    strg = Cstrg;
    if( H5free_memory(Cstrg) < 0) {
        reportError("freeing memory from HDF5 library failed");
    }
}

} // namespace h5pp

////////////////////////////////////////////////////////////////////////////////
//
// Location
//
////////////////////////////////////////////////////////////////////////////////
namespace {
herr_t collect_attr_names(hid_t, const char*name, const H5A_info_t*,
    void*names)
{
    reinterpret_cast< vector<string> *>(names)->emplace_back(name);
    return 0;
}

}  // namespace {
//
namespace h5pp {

// NOTE name == parent?  local name : file name
Location::Location(id_type id, const Location*parent, string const&name)
  : Object(id)
  , local_name(parent==nullptr? string("") : name) 
  , full_name (parent==nullptr? name : (parent->full_name + '/' + name))
{
    if(debugDepth >=8 )
	debugReport(8,"Location::Location(id=" + to_string(id) + ",parent=" +
	    to_string(parent) + ",name='" + name + "'): local_name='" +
	    local_name + "' full_name='" + full_name + "' fileName()='" +
	    fileName() + "' this=" + to_string(this));
}

// adapted to change in more recent version
Location::Info Location::info() const
{
    H5O_info_t  i;
    silenceNested silent;
    if(H5Oget_info(getId(), &i) < 0) {
	reportError("unable to get location information");
    }
    return{i.addr, i.rc, i.btime, i.atime, i.mtime, i.num_attrs};
}

// tested 21-01-2020
string Location::fileName() const
{
    return get_name(H5Fget_name,getId());
}

// tested 21-01-2020
Attribute Location::createAttribute(string const&name,
    DataSpace const&space, DataType const&dtype)
{
    silenceNested silent;
    auto attr_id = H5Acreate2(getId(), name.c_str(), dtype.getId(),
	space.getId(), H5P_DEFAULT, H5P_DEFAULT);
    if( attr_id < 0 ) {
        reportError("unable to create attribute '" + name +
	    "' at '" + fullName() + '\'');
    }
    return {attr_id};
}

// tested 21-01-2020
bool Location::hasAttribute(string const&name) const
{
    silenceNested silent;
    auto ans = H5Aexists(getId(), name.c_str());
    if( ans < 0 ) {
        reportError("querying for attribute '" + name +
	    "' at '" + fullName() + "' failed");
    }
    return bool(ans);
}
    
// tested 21-01-2020
Attribute Location::openAttribute(string const&name) const
{
    silenceNested silent;
    auto attr_id = H5Aopen(getId(), name.c_str(), H5P_DEFAULT);
    if( attr_id < 0 ) {
        reportError("unable to open attribute '" + name +
	    "' at '" + fullName() + '\'');
    }
    return {attr_id};
}

// tested 21-01-2020
vector<string> Location::attributeNames() const
{
    vector<string> attr_names;
    if( H5Aiterate(getId(), H5_INDEX_NAME, H5_ITER_INC, nullptr, 
	    collect_attr_names, &attr_names) < 0) {
        reportError("iterating attributes of '" + fullName() + "' failed");
    }
    return attr_names;
}

////////////////////////////////////////////////////////////////////////////////
//
// DataSet
//
////////////////////////////////////////////////////////////////////////////////

DataSetCreateProperties DataSet::createProperties() const
{
    silenceNested silent;
    const auto plist_id = H5Dget_create_plist(getId());
    if( plist_id < 0 ) {
	reportError("unable to obtain creation properties for data set");
    }
    return {plist_id};
    
}

//
DataSetAccessProperties DataSet::accessProperties() const
{
    silenceNested silent;
    const auto plist_id = H5Dget_access_plist(getId());
    if( plist_id < 0 ) {
	reportError("unable to obtain access properties for data set");
    }
    return {plist_id};
    
}

//
DataSpace DataSet::space() const
{
    silenceNested silent;
    const auto space_id = H5Dget_space(getId());
    if( space_id < 0 ) {
	reportError("unable to obtain data space for data set");
    }
    return DataSpace(space_id);
}

//
void DataSet::extend(size_type nExt, const size_type*newExtent, bool warnReduce)
{
    if(nExt == 0) {
	return;
    }
    silenceNested silent;
    // obtain current and maximum extents
    const auto space_id = H5Dget_space(getId());
    if( space_id < 0 ) {
	reportError("unable to obtain data space for data set");
    }
    const auto ndimH = H5Sget_simple_extent_ndims(space_id);
    if(ndimH < 0) {
	reportError("unable to get number of data space dimensions");
    }
    if( ndimH==0 ) {
	return;
    }
    const auto ndim = size_type(ndimH);
    vector<hsize_t> dims(ndim+ndim);
    if( H5Sget_simple_extent_dims(space_id,dims.data(),dims.data()+ndim) < 0) {
	reportError("unable to get maximum extent of data space");
    }
    // set new current extent, throw if it exceeds maximum extent
    bool isReducing = false;
    for(size_type dim=0; dim!=nExt; ++dim) {
	if(newExtent[dim] > dims[ndim+dim]) {
	    throw Exception("DataSet: cannot extend in dimension " +
		to_string(dim) + " beyond " + to_string(dims[ndim+dim]) +
		" elements (" + to_string(newExtent[dim]) + " requested)");
	} else {
	    isReducing = isReducing || (newExtent[dim] < dims[dim]);
	    dims[dim]  = newExtent[dim];
	}
    }
    if(isReducing && warnReduce) {
	cerr << "h5pp::DataSet::extend(): reducing extent: "
	     << "will remove data on file" << std::endl;
    }
    if( H5Dset_extent(getId(), dims.data()) < 0) {
	reportError("unable to extend DataSet");
    }
}

//
DataType DataSet::type() const
{
    silenceNested silent;
    const auto type_id = H5Dget_type(getId());
    if( type_id < 0 ) {
	reportError("unable to obtain data type for data set");
    }
    const auto copied = H5Tcopy(type_id);
    if( copied < 0 ) {
	reportError("unable to copy data type for data set");
    }
    return {copied};
}

//
void DataSet::read(void*buffer, DataSpace const&memSpace,
    DataType const&memType, DataSpace::Selection const&fileSelect) const
{
    if(buffer==nullptr)
	throw Exception("DataSet['"+fullName()+"']::read(void*buffer=0x0)");
    silenceNested silent;
    if( memType.getClass() == DataType::Class::String ) {
	throw Exception("DataSet::read(): memType is string type "
	    "-- use read(string*) for reading strings");
    }
    if(fileSelect.trivial()) {
	if( H5Dread(getId(), memType.getId(), memSpace.getId(),
		H5S_ALL, H5P_DEFAULT, buffer) < 0) {
	    reportError("reading buffer of " + memType.description() +
		" from data set '" + fullName() + "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] read " +
	    to_string(memSpace.numElements()) + ' ' + memType.description());
    } else {
	auto fileSpace = space();
	fileSpace.select(fileSelect);
	if( memSpace.numSelected() != fileSpace.numSelected()) {
	    cerr<<"DataSet::read(): it appears that the number of selected "
		<<"elements does not match. Attempting H5Dread() now ...\n";
	}
	if( H5Dread(getId(), memType.getId(), memSpace.getId(),
		fileSpace.getId(), H5P_DEFAULT, buffer) < 0) {
	    reportError("reading buffer of " + memType.description() +
		" from data set '" + fullName() + "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] read " +
	    to_string(memSpace.numElements()) + " of " +
	    to_string(fileSpace.numElements())  + ' ' + memType.description());
    }
}

//
void DataSet::read(string*buffer, DataSpace const&memSpace,
    DataSpace::Selection const&fileSelect) const
{
    //
    // regarding UTF-8 vs ASCII see
    // https://portal.hdfgroup.org/display/HDF5/Using+UTF-8++Encoding+in+HDF5+Applications
    //
    if(buffer==nullptr)
	throw Exception("DataSet['"+fullName()+"']::read(string*buffer=0x0)");
    silenceNested silent;
    // 1  ensure data on file are strings and obtain appropriate memory-type
    auto type = H5Dget_type(getId());
    if( type < 0 ) {
	reportError("DataSet::read(string*): unable to obtain file datatype");
    }
    const auto clss = H5Tget_class(type);
    if( clss != H5T_STRING ) {
	throw Exception(string("DataSet::read(string*): data on file are ") +
	    (clss == H5T_INTEGER? "integers" :
	     clss == H5T_FLOAT? "floating-point numbers" :
	     clss == H5T_TIME? "times" : "not strings"));
    }
    const auto vlen = H5Tis_variable_str(type);
    if( vlen < 0 ) {
	reportError("DataSet::read(string*): unable to enquire whether "
	    "file holds variable-length strings");
    }
    if( vlen == 0 ) {
	type = H5Tcopy(type);
	if( type < 0 ) {
	    reportError("DataSet::read(string*): "
		"unable to create memory datatype");
	}
	if( H5Tset_size(type, H5T_VARIABLE) < 0 ) {
	    reportError("DataSet::read(string*): "
		"unable to set memory datatype to variable-length");
	}
    }
    // 2  create buffer of ndata C-style strings (char*), initialised to null
    const auto ndata = product(memSpace.extent());
    vector<char*> Cbuffer(ndata,nullptr);
    // 3  call to HDF5: reads selected C-style strings (and allocates them)
    if(fileSelect.trivial()) {
	if( H5Dread(getId(), type, memSpace.getId(), H5S_ALL, 
		H5P_DEFAULT, static_cast<void*>(Cbuffer.data())) < 0 ) {
	    if( vlen == 0 ) {
		H5Tclose(type);
	    }
	    reportError("reading buffer of strings from data set '" + fullName()
		+ "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] read " +
	    to_string(memSpace.numElements()) +  " strings");
    } else {
	auto fileSpace = space();
	fileSpace.select(fileSelect);
	if( memSpace.numSelected() != fileSpace.numSelected()) {
	    cerr << "DataSet::read(string*): the number of selected elements "
		 << "does not appear to match. Attempting H5Dread() now ...\n";
	}
	if( H5Dread(getId(), type, memSpace.getId(), fileSpace.getId(),
		H5P_DEFAULT, static_cast<void*>(Cbuffer.data())) < 0 ) {
	    if( vlen == 0 ) {
		H5Tclose(type);
	    }
	    reportError("reading buffer of strings from data set '" + fullName()
		+ "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] read " +
	    to_string(memSpace.numElements()) +  " of " +
	    to_string(fileSpace.numElements())  + " strings");
    }
    // 4  convert to strings (only those char* which aren't null, because they
    //    have been selected and hence read)
    for(size_type i=0; i!=ndata; ++i) {
	if(Cbuffer[i] != nullptr) {
	    buffer[i] = string(Cbuffer[i]);
	}
    }
    // 5  free memory for C-style strings
    if( H5Dvlen_reclaim(type, memSpace.getId(), H5P_DEFAULT,
	    static_cast<void*>(Cbuffer.data())) < 0 ) {
	reportError("HDF5 failed to reclaim memory from variable-length read");
    }
    if( vlen == 0 ) {
	H5Tclose(type);
    }
}

//
void DataSet::write(const void*buffer, DataSpace const&memSpace,
    DataType const&memType, DataSpace::Selection const&fileSelect)
{
    if(buffer==nullptr)
	throw Exception("DataSet['"+fullName()+"']::write(void*buffer=0x0)");
    silenceNested silent;
    if( memType.getClass() == DataType::Class::String ) {
	throw Exception("DataSet::write(): memType is string type "
	    "-- use write(string*) for writing strings");
    }
    if(fileSelect.trivial()) {
	if( H5Dwrite(getId(), memType.getId(), memSpace.getId(),
		H5S_ALL, H5P_DEFAULT, buffer) < 0) {
	    reportError("writing buffer of " + memType.description() +
		" to data set '" + fullName() + "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] written " +
	    to_string(memSpace.numElements()) + ' ' + memType.description());
    } else {
	auto fileSpace = space();
	fileSpace.select(fileSelect);
	if( memSpace.numSelected() != fileSpace.numSelected()) {
	    cerr<<"DataSet::write(): it appears that the number of selected "
		<<"elements does not match. Attempting H5Dwrite() now ...\n";
	}
	if( H5Dwrite(getId(), memType.getId(), memSpace.getId(),
		fileSpace.getId(), H5P_DEFAULT, buffer) < 0) {
	    reportError("writing buffer of " + memType.description() +
		" to data set '" + fullName() + "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] written " +
	    to_string(memSpace.numElements()) + " of " +
	    to_string(fileSpace.numElements()) + ' ' + memType.description());
    }
}

//
void DataSet::write(const string*buffer, DataSpace const&memSpace,
    DataSpace::Selection const&fileSelect, bool utf8)
{
    if(buffer==nullptr)
	throw Exception("DataSet['"+fullName()+"']::write(string*buffer=0x0)");
    silenceNested silent;
    // 1  obtain ndata = total size of memory space
    const auto extent = memSpace.extent();
    const auto ndata = product(extent);
    // 2  create buffer of ndata const char* pointing to the strings
    vector<const char*> Cbuffer(ndata+1);
    for(size_type i=0; i!=ndata; ++i) {
	Cbuffer[i] = buffer[i].c_str();
    }
    Cbuffer.back() = nullptr;
    // 3  call to HDF5
    const auto memType = DataType::For<string>(utf8);
    if(fileSelect.trivial()) {
	if( H5Dwrite(getId(), memType.getId() , memSpace.getId(),
		H5S_ALL, H5P_DEFAULT,
		static_cast<const void*>(Cbuffer.data())) < 0) {
	    reportError("writing buffer of strings to data set '" +
		fullName() + "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] written " +
	    to_string(memSpace.numElements()) + " strings");
    } else {
	auto fileSpace = space();
	fileSpace.select(fileSelect);
	if( memSpace.numSelected() != fileSpace.numSelected()) {
	    cerr<<"DataSet::write(): it appears that the number of selected "
		<<"elements does not match. Attempting H5Dwrite() now ...\n";
	}
	if( H5Dwrite(getId(), memType.getId() , memSpace.getId(),
		fileSpace.getId(), H5P_DEFAULT,
		static_cast<const void*>(Cbuffer.data())) < 0) {
	    reportError("writing buffer of strings to data set '" +
		fullName() + "' failed");
	}
	debugReport(6,"DataSet['" + fullName() + "'] written " +
	    to_string(memSpace.numElements()) + " of " +
	    to_string(fileSpace.numElements()) + " strings");
    }
}

} // namespace h5pp

////////////////////////////////////////////////////////////////////////////////
//
// Group
//
////////////////////////////////////////////////////////////////////////////////
namespace {

struct loc_names { Group::nameSet dst,grp; };

// tested 23-01-2020
herr_t collect_location_names(hid_t loc_id, const char*name,
    const H5L_info_t*, void*data)
{
    auto child_id = H5Oopen(loc_id, name, H5P_DEFAULT);
    if( child_id < 0 ) {
	return -1;
    }
    auto ptr = reinterpret_cast<loc_names*>(data);
    switch(details::type(child_id)) {
    case Object::Type::DataSet:
	ptr->dst.emplace(name);
	break;
    case Object::Type::Group:
	ptr->grp.emplace(name);
	break;
    default: break;  // suppress warnings about enum values not handled
    }
    H5Oclose(child_id);
    return 0;
}
//
void check_rank_match_chunk_extent(const int space_ndim, string const&name,
    id_type create)
{
    if( has_chunked_layout(create,propertyType::datasetCreate) ) {
	const auto chunk_ndim = H5Pget_chunk(create, 0, nullptr);
	if( chunk_ndim < 0 ) {
	    reportError("Group::createDataSet('" + name +
		"'): unable to query rank of chunk sizes");
	}
	if( chunk_ndim != space_ndim ) {
	    reportError("Group::createDataSet('" + name +
		"'): dataSpace::rank()=" + to_string(space_ndim) + " != " +
		to_string(chunk_ndim) +
		"=rank of chunks in dataSetCreationProperties");
	}
    }
}
//
void check_rank_match_chunk_extent(string const&name,
    id_type space, id_type create)
{
    const auto space_ndim = H5Sget_simple_extent_ndims(space);
    if( space_ndim < 0 ) {
	reportError("Group::createDataSet(" + name +
	    "): unable to get number of data space dimensions");
    }
    return check_rank_match_chunk_extent(space_ndim,name,create);
}

} // namespace {
//
namespace h5pp {

void Group::retrieve_location_names() const
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot list locations of invalid " + typeName());
    }
    location_names.reset(new loc_names);
    silenceNested silent;
    if( H5Literate(getId(), H5_INDEX_NAME, H5_ITER_INC, nullptr, 
	    collect_location_names, location_names.get()) < 0) {
        reportError("retrieving data-set and group names in group '"
	    + fullName() + "' failed");
    }
}

//
Location::Info Group::subLocationInfo(string const&name) const
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot obtain sub-location info from invalid " +
	    typeName());
    }
    H5O_info_t i;
    if( H5Oget_info_by_name(getId(), name.c_str(), &i, H5P_DEFAULT) < 0) {
	reportError("unable to get sub-location information from group '" +
	    fullName() + '\'');
    }
    return{i.addr, i.rc, i.btime, i.atime, i.mtime, i.num_attrs};
}

//
id_type Group::create_dataset_nonextendible(string const&name, id_type space,
    id_type type, id_type create, id_type access)
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot create data set from invalid " + typeName());
    }
    silenceNested silent;
    // check for mismatch between ranks of data space and chunk table
    check_rank_match_chunk_extent(name,space,create);
    // create data set
    const auto dset = H5Dcreate2(getId(),name.c_str(),type,space,H5P_DEFAULT,
	create,access);
    if( dset < 0 ) {
	reportError("unable to create non-extendible data set '" +
	    fullName() + '/' + name + '\'');
    }
    if(location_names) {
	location_names->dst.insert(name);
    }
    debugReport(4,"non-extendible Dataset '" + name + "' created: id = " +
	to_string(dset) + " from Group " + to_string(this));
    return dset;
}

//
id_type Group::create_dataset_extendible(string const&name, id_type space,
    id_type type, id_type create, id_type access)
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot create data set from invalid " + typeName());
    }
    silenceNested silent;
    const auto DSCreate = propertyType::datasetCreate;
    // obtain rank of data space
    const auto rank = H5Sget_simple_extent_ndims(space);
    if(rank < 0) {
	reportError("Group::createDataSet('" + name +
	    "'): unable to get number of data space dimensions");
    }
    // check for mismatch between ranks of data space and chunk table
    check_rank_match_chunk_extent(rank,name,create);
    // obtain data space extent and maximum extent
    const auto ndim = size_type(rank);
    vector<hsize_t> temp(2*ndim);
    hsize_t *iniExt = temp.data(), *maxExt = iniExt + ndim;
    if( H5Sget_simple_extent_dims(space, iniExt, maxExt) < 0) {
	reportError("Group::createDataSet('" + name +
	    "') unable to get initial and maximum extent of data space");
    }
    // check whether the data space is extendible
    bool extendible = false;
    string extName, maxName;
    for(size_type dim=0; dim!=ndim && !extendible; ++dim) {
	extendible = iniExt[dim] < maxExt[dim];
	if(dim) extName += ":";  extName += to_string(iniExt[dim]);
	if(dim) maxName += ":";  maxName += to_string(maxExt[dim]);
    }
    debugReport(6,"Group::createDataSet('" + name + "'): "
	"data space initial and maximum extension = [" +
	extName + "] and [" + maxName + "] is " +
	(rank==H5S_SCALAR? "scalar":"simple"));
    id_type dset = -1;
    // 1   not extendible:
    if(!extendible) {
	debugReport(6,"Group::createDataSet('" + name + "'): "
	    "data space not extendible: will create non-extendible data set "
	    "with " + (create==H5P_DEFAULT? "default":"non-default") +
	    " data-set-creation property list");
	dset = H5Dcreate2(getId(),name.c_str(),type,space,H5P_DEFAULT,create,
	    access);
    }
    // 2   extendible, but default creation property list:
    //     make a suitable creation property list
    else if (create == H5P_DEFAULT) {
	create = H5Pcreate(convert_plist_type(DSCreate));
	if( create < 0 ) {
	    reportError("Group::createDataSet('" + name +
		"') unable to create " + propertiesName(DSCreate));
	}
	if( H5Pset_layout(create, H5D_CHUNKED) < 0 ) {
	    reportError("Group::createDataSet('" + name +
		"') unable to set chunked layout");
	}
	// use chunk[] = iniExt[], except in lowest dimension
	hsize_t numH = 1;
	for(size_t dim=1; dim < ndim; ++dim)
	    numH *= iniExt[dim];
	iniExt[0] = numH>hsize_t(1024)?
	    1 : std::max(hsize_t(1), std::min(iniExt[0], (1024/numH)));
	if( H5Pset_chunk(create, int(ndim), iniExt) < 0 ) {
	    reportError("Group::createDataset('" + name +
		"') unable to set chunk sizes");
	}
	dset = H5Dcreate2(getId(),name.c_str(),type,space,H5P_DEFAULT,create,
	    access);
	if( H5Pclose(create) < 0 ) {
	    reportError("Group::createDataSet('" + name +
		"') unable to close " + propertiesName(DSCreate));
	}
    }
    // 3   extendible, but non-default property list provided
    // 3.1 check for chunked layout
    else if( H5Pget_layout(create) != H5D_CHUNKED ) {
	throw Exception("Group::createDataSet('" + name +
	    "'): for extendible data sets the DataSetCreateProperties "
	    " must contain Chunked");
    }
    // 3.2 check for correct chunk rank
    else {
	const auto cdim = H5Pget_chunk(create, 0, nullptr);
	if(cdim < 0) {
	    reportError("Group::createDataset('"  + name +
		"') unable to get chunk rank");
	}
	if(size_type(cdim) != ndim) {
	    throw Exception("Group::createDataSet('" + name +
		"'): extendible data space with rank " + to_string(ndim) +
		" but DataSetCreateProperties provides chunks with rank " +
		to_string(cdim));
	}
	dset = H5Dcreate2(getId(),name.c_str(),type,space,H5P_DEFAULT,create,
	    access);
    }
    if( dset < 0 ) {
	reportError("unable to create data set '" +
	    fullName() + '/' + name + '\'');
    }
    if(location_names) {
	location_names->dst.insert(name);
    }
    debugReport(4,"extendible Dataset '" + name + "' created: id = " +
	to_string(dset) + " from Group " + to_string(this));
    return dset;
}

//
DataSet Group::openDataSet(string const&name,
    DataSetAccessProperties const&access_props) const
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot create data set from invalid " + typeName());
    }
    silenceNested silent;
    const auto dset = H5Dopen2(getId(), name.c_str(), access_props.getId());
    if( dset < 0 ) {
	reportError("unable to open data set '" + fullName() + '/' +
	    name + '\'');
    }
    debugReport(4,"Dataset '" + name + "' opened: id = " +
	to_string(dset) + " from Group " + to_string(this));
    return {dset,this,name};
}

/*
  Group Group::createLink(std::string const&name, Group const&targe,
  bool relative)
  {
  if( !is_valid(getId()) ) {
  throw Exception("Group::createLink() called on invalid group/file");
  }
  if( !is_valid(file.getId()) ) {
  throw Exception("Group::createLink() called with invalid group/file");
  }
  // 1  target is in the same file
  if(this->fileName() == target.fileName()) {


  }
  // 2  target is another file
  else if(target.isFile()) {


  }
  // 3  target is a group in another file
  else {


  }
  }
*/

//
Group Group::createGroup(string const&name)
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot create group in invalid " + typeName());
    }
    silenceNested silent;
    const auto grp = H5Gcreate2(getId(), name.c_str(), H5P_DEFAULT,
	H5P_DEFAULT, H5P_DEFAULT);
    if( grp < 0 ) {
	reportError("failed to create new group '" + fullName() + '/' +
	    name + '\'');
    }
    if(location_names) {
	location_names->grp.insert(name);
    }
    debugReport(4,"Group '" + name + "' created: id = " + to_string(grp) +
	" from Group " + to_string(this));
    return {grp,this,name,true};
}

//
Group Group::openGroup(string const&name) const
{
    if( !is_valid(getId()) ) {
	throw Exception("cannot open group in invalid " + typeName());
    }
    silenceNested silent;
    const auto grp = H5Gopen2(getId(), name.c_str(), H5P_DEFAULT);
    if( grp < 0 ) {
	reportError("failed to open group '" + fullName() + '/' + name + '\'');
    }
    debugReport(4,"Group '" + name + "' opened: id = " + to_string(grp) +
	" from Group " + to_string(this));
    return {grp,this,name,false};
}

//
void Group::flush()
{
    debugReport(4,"Group::flush() [localName='" + localName() +
	"', fullName='" + fullName() + "']: id = " + to_string(getId()) +
	" this=" + to_string(this));
    if( is_valid(getId()) ) {
	silenceNested silent;
	if( isFile() ) {
	    if( H5Fflush(getId(), H5F_SCOPE_LOCAL) < 0) {
		reportError("failed to flush file data to disc for '" +
		    fileName() + "' (id=" + to_string(getId()) + " this=" +
		    to_string(this) + ')');
	    }
	} else {
	    if( H5Gflush(getId()) < 0) {
		reportError("failed to flush group data to disc for '" +
		    fullName() + "' (id=" + to_string(getId()) + " this="
		    + to_string(this) + ')');
	    }
	}
    }
}

//
void Group::reset_file(id_type id, string const&file_name,
    bool created, bool nothrow)
{
    reset_identifier(id, nothrow);
    Location::local_name = "";
    Location::full_name = file_name;
    try {
	location_names.reset(created? new loc_names : nullptr);
    } catch(...) {
	if(!nothrow)
	    throw;
    }
    debugReport(4,"Group::reset_file(id=" + to_string(id) + ",file_name='" +
	file_name + "') created=" + (created? "true":"false") + " this=" +
	to_string(this) + " localName='" + localName() + "' fullName()='" +
	fullName() + '\'');
}

////////////////////////////////////////////////////////////////////////////////
//
// File
//
////////////////////////////////////////////////////////////////////////////////

File::File()
  : Group(Object::invalid_id, nullptr, "", false)
{
    debugReport(4,"File default constructed: this=" + to_string(this));
}

//
File::File(string const&name, unsigned openFlags,
    FileAccessProperties const&accessProps)
  : Group(open(name, openFlags, accessProps.getId()),
      nullptr, "", create_file(openFlags))
{
    debugReport(4,"File constructed from file '" + name + "' flags=" +
	to_string(openFlags) + " accessProps &" + to_string(&accessProps) +
	" this=" + to_string(this) + " localName()='" + localName() +
	"' fullName()='" + fullName() + '\'');
}

//
bool File::readOnly() const
{
    if( !is_valid(getId()) ) {
	throw Exception("file is not open: cannot enquire read-only status");
    }
    unsigned access_flag = 0;
    if( H5Fget_intent(getId(), &access_flag) < 0 ) {
	reportError("failed to obtain file access flag");
    }
    return
	access_flag == H5F_ACC_RDONLY ||
	access_flag ==(H5F_ACC_RDONLY | H5F_ACC_SWMR_READ); 
}

//
size_type File::size() const
{
    if( !is_valid(getId()) ) {
	return 0;
    }
    hsize_t hsize = 0;
    if( H5Fget_filesize(getId(), &hsize) < 0 ) {
	reportError("failed to obtain file size");
    }
    return size_type(hsize);
}

//
bool File::isHDF5(string const&name)
{
    const auto ans = H5Fis_hdf5(name.c_str());
    if(ans < 0) {
	reportError("File::isHDF5('" + name + "' failed");
    }
    return ans > 0;
}

//
id_type File::open(string const&name, unsigned openFlags, id_type accessProps)
{
    debugReport(6,"File::open('" + name + "',flags=" + to_string(openFlags));
    id_type file = Object::invalid_id;
    silenceNested silent;
    // 1  create new file: H5Fcreate
    if(create_file(openFlags)) {
	file = H5Fcreate(name.c_str(),
	    (openFlags & Truncate)? H5F_ACC_TRUNC : H5F_ACC_EXCL,
	    H5P_DEFAULT, accessProps);
	if( file < 0 ) {
	    reportError("failed to create file '" + name + '\'');
	}
    }
    // 2  open existing file: H5Fopen
    else {
	file = H5Fopen(name.c_str(),
	    (openFlags & ReadWrite) ? H5F_ACC_RDWR : H5F_ACC_RDONLY,
	    accessProps);
	if( file < 0 ) {
	    reportError("failed to open file '" + name + '\'');
	}
    }
    debugReport(4,"File '" + name + "' opened with flags " +
	to_string(openFlags) + ": id = " + to_string(file));
    return file;
}

//
FileAccessProperties File::accessProperties() const
{
    if( !is_valid(getId()) ) {
	return {};
    }
    auto plist = H5Fget_access_plist(getId());
    if( plist < 0 ) {
	reportError("failed to obtain file-access properties");
    }
    return {plist};
}

} // namespace h5pp
