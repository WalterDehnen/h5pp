# README #

### What is h5pp for? ###

* **h5pp** is a simple C++ interface to HDF5

* **Why not the official HDF5 C++ interface?**

That interface is written in a confusing non-portable way, overly complicated, poorly documented,
not threadsafe, and incomplete w/o ability to extend.

* **Why not another C++ interface?**

These don't seem to be well supported, incomplete, or faulty.
HighFive, for instance, appears to have stopped development 2017 (uses
deprecated HDF5 features), leaks, crashes (File::numAttributes), and does
not support low-level I/O. 

### How do I get set up? ###

* **Set up**

download/pull, then `make` to create the library `libh5pp.so`

* **Dependencies**
the HDF5 C API and library (`libhdf5.so`), version ≥ 1.10

* Tests:
tests cases to be added, contributions welcome

### Contribution guidelines and requests ###

* **Configuration**

Currently there is no configuration beyond the makefile designed for the gcc/clang compilers.
Configuration via `configure` or `cmake` to auto-detect C++ compiler and HDF5 version needed.

* **Documentation**

Currently only within the code. Doxygen documentation needed.

* **Tests and examples**

A test base and example code are needed. I anticipate separate tests for each method.

* **Code review**

Code reviews are welcome.

* **Code dependencies**

`h5pp.hpp` (and files it includes) to only depend on the C++ standard library

`h5pp.cpp` in addition to depend only on `libhdf5.so` and associated C header files

* **programming language**
C++ standard 2011

### Who do I talk to? ###

* <wdehnen64@gmail.com>