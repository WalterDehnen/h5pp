/*
 *   Copyright (c), 2020, Walter Dehnen <wdehnen64@gmail.com>
 *
 *  Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE_1_0.txt or copy at
 *            http://www.boost.org/LICENSE_1_0.txt)
 *
 */

#include "h5pp.hpp"
#include <vector>
#include <iostream>
#include <iomanip>
#include <array>

/*
  
 */

// a point in 3D space
using point = std::array<double,3>;

int main()
{
    // 1   create file and a data set extendable in the first dimension
    {
	// 1.1   create a new file named 'test2_h5pp.h5'
	//       truncating any previous file of the same name
	h5pp::File file("test2_h5pp.h5", h5pp::File::OverWriting);

	std::cout << "file 'test2_h5pp.h5' created for writing\n";
	// 1.2   create some point data
	std::vector<point> data;
	for(size_t i=0; i!=10; ++i)
	    data.push_back(point{{0.2*i,0.1*i*i,4-0.3*i}});

	// The simplest way to read/write point data, is to read/write
	// 'double' with the an extent of 3 in the last dimension. Thus, our
	// data here have extent 10x3.

	// 1.3   create a suitable data type
	auto type = h5pp::DataType::For<double>();

	// 1.4   create a suitable extendable data space
	h5pp::DataSpace space({data.size(),3}, h5pp::DataSpace::FirstDim);

	// 1.5   create a corresponding data set with initial extent 10x3
	auto data_set = file.createDataSet("data", space, type);

	// 1.6   create a data space describing the memory layout
	h5pp::DataSpace memory_space({data.size(),3});

	// 1.6   write data
	data_set.write(data.data(), {data.size(),3}, type);
	
	std::cout << "data written to dataset 'test2_h5pp.h5/data':\n";
	for(auto const&x : data)
	    std::cout << "     "
		      << std::setw(10) << x[0] << ' '
		      << std::setw(10) << x[1] << ' '
		      << std::setw(10) << x[2] << '\n';
	std::cout << std::endl;

	// 1.7   end of scope: data_set and file are flushed and closed
    }

    std::cout << "file 'test2_h5pp.h5' closed\n";

    // 2   re-open the same file and add more point data
    {
	// 2.1   open the same file for reading and writing
	h5pp::File file("test2_h5pp.h5", h5pp::File::OpenForWriting);

	std::cout << "file 'test2_h5pp.h5' re-opened for further writing\n";
	// 2.2   open the first data set
	auto data_set = file.openDataSet(*(file.dataSetNames().begin()));

	// 2.3   enquire its dataspace extent and maximum extent
	h5pp::size_table extent, max_extent;
	data_set.space().getExtent(extent,max_extent);
	const auto rank = extent.size();
	if(rank!=2) {
	    std::cerr << "ERROR: data set has rank " << rank
		      << " but expected rank=2\n";
	    return -1;
	}
	if(max_extent[0] <= extent[0]) {
	    std::cerr << "ERROR: data set is not extendible:"
		      << " extent[0]=" << extent[0]
		      << " max_extend[0]=" << max_extent[0] << "\n";
	    return -1;
	}
	// 2.4   extend data space by up to 10 points
	const auto num_add = std::min(max_extent[0]-extent[0],size_t(10));
	data_set.extend(extent[0]+num_add);

	// 2.5   create 10 more data points for output
	std::vector<point> data;
	for(size_t i=extent[0],j=0; j!=num_add; ++j,++i)
	    data.push_back(point{{0.2*i,0.1*i*i,4-0.3*i}});

	// 2.6   create a data space describing the memory layout
	h5pp::DataSpace memory_space({num_add,3});

	// 2.7   write the new data behind the previous data
	//       the last argument selects num_add points starting at extent[0]
	//       to write into.
	data_set.write(data.data(), memory_space, h5pp::DataType::For<double>(),
	    h5pp::DataSpace::Selection(extent[0],num_add));

	std::cout << "data added to dataset 'test2_h5pp.h5/data':\n";
	for(auto const&x : data)
	    std::cout << "     "
		      << std::setw(10) << x[0] << ' '
		      << std::setw(10) << x[1] << ' '
		      << std::setw(10) << x[2] << '\n';
	std::cout << std::endl;

	// 2.8   end of scope: data_set and file are flushed and closed
    }

    std::cout << "file 'test2_h5pp.h5' closed\n";

    // 3   re-open the same file for reading and report the data
    {
	// 3.1   open the same file for reading only
	h5pp::File file("test2_h5pp.h5");

	std::cout << "file 'test2_h5pp.h5' opened for reading\n";
	// 3.2   open the first data set
	auto data_set = file.openDataSet(*(file.dataSetNames().begin()));

	// 3.3   obtain data space
	auto extent = data_set.space().extent();
	
	// 3.4   read data
	std::vector<point> data(extent[0]);
	data_set.read(data.data(), extent, h5pp::DataType::For<double>());

	std::cout << "data read from dataset 'test2_h5pp.h5/data':\n";
	for(auto const&x : data)
	    std::cout << "     "
		      << std::setw(10) << x[0] << ' '
		      << std::setw(10) << x[1] << ' '
		      << std::setw(10) << x[2] << '\n';
	std::cout << std::endl;
    }
}
	
	
	

	
	
	
