/*
 *   Copyright (c), 2020, Walter Dehnen <wdehnen64@gmail.com>
 *
 *  Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE_1_0.txt or copy at
 *            http://www.boost.org/LICENSE_1_0.txt)
 *
 */

#include "h5pp.hpp"
#include <vector>
#include <iostream>

/*
  writing and reading a dataset in different ways, one with attribute
 */

int main()
{
    // 1   create file and write data sets
    //     put all output operations into a separate scope, see also 1.5 below
    {
	// 1.1   create a new file named 'test1_h5pp.h5'
	//       truncating any previous file of the same name
	h5pp::File file("test1_h5pp.h5", h5pp::File::OverWriting);

	// 1.2   create some data of floating-point type
	std::vector<double> data;
	for(size_t i=0; i!=10; ++i)
	    data.push_back(0.4*i-2);

	std::cout << "data created: ";
	for(auto x : data)
	    std::cout << x << ' ';
	std::cout << std::endl;

	// 1.3   write our data into a dataset named 'data1'
	file.writeDataSet("data1",data);

	// 1.4   write the same data, but give them an attribute 'time'
	// 1.4.1 create a dataset named 'data2' suitable for our data
	auto data_set = file.createDataSetFor("data2", data);

	// 1.4.2 write attribute time 
	double time = 0.2341;
	data_set.writeAttribute("time",time);

	// 1.4.3 write our data
	data_set.write(data);

	// 1.5   create data set of strings
	std::vector<std::string> strings;
	strings.push_back("first string");
	strings.push_back("second string");
	strings.push_back("final string");

	// 1.6   create and write strings with one call
	file.writeDataSet("data3",strings);

	// 1.7   end of scope: data_set and file are closed
	//       preferrable to file.close() as it avoids a dangling data_set
    }

    // 2     open same file for reading and read data back in
    h5pp::File file("test1_h5pp.h5");

    // 2.1   list dataset names
    std::cout << "file has data sets: ";
    for(const auto&name : file.listDataSetNames())
	std::cout << '\'' << name << "' ";
    std::cout << std::endl;

    // 2.1   read data in dataset 'data1' as double
    std::vector<double> data_double;
    file.readDataSet("data1", data_double);
    
    std::cout << "data1 read as double: ";
    for(auto x : data_double)
	std::cout << x << ' ';
    std::cout << std::endl;

    // 2.2   read data in dataset 'data2' as float
    std::vector<float> data_float;
    file.readDataSet("data2", data_float);
    
    std::cout << "data2 read as float: ";
    for(auto x : data_float)
	std::cout << x << ' ';
    std::cout << std::endl;
    
    // 2.3   open dataset 'data2' and work on it
    auto data_set = file.openDataSet("data2");

    // 2.3.1 obtain data space
    auto space = data_set.space();
    auto extent = space.extent();

    std::cout << "data2 has data space extent ";
    for(size_t i=0; i!=extent.size(); ++i)
	std::cout << (i?"x":"") << extent[i];
    std::cout << std::endl;

    // 2.3.2 obtain data type
    auto type = data_set.dataType();
    std::cout << "data2 has data type '" << type.description() << '\''
	      << std::endl;

    // 2.3.3 list attribute names
    std::cout << "data set 'data2' has attributes: ";
    for(const auto&name : data_set.listAttributeNames())
	std::cout << '\'' << name << "' ";
    std::cout << std::endl;

    // 2.3.4 read attribute 'time'
    double time;
    data_set.readAttribute("time",time);

    std::cout << "attribute 'time' has value " << time
	      << std::endl;

    // 2.3.5 read data in data set as unsigned
    std::vector<unsigned> data_unsigned;
    data_set.read(data_unsigned);
    
    std::cout << "data2 read as unsigned: ";
    for(auto x : data_unsigned)
	std::cout << x << ' ';
    std::cout << std::endl;

    // 2.4   read data in dataset 'data3' with one call
    std::vector<std::string> strings;
    file.readDataSet("data3",strings);

    // 2.4.1 report strings
    std::cout << "strings read from data set 'data3':\n";
    for(const auto&str : strings)
	std::cout << '\'' << str << "'\n";
    
    // close all open objects by implicit call of destructors at end of scope
}
