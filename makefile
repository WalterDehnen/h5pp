
default: 	libh5pp.so

CXX		:= clang++
CXXFLAGS	:= -std=c++11 -O2 -Wall

libh5pp.so	: src/h5pp.cpp include/h5pp.hpp \
			include/bits/h5pp_traits.hpp \
			include/bits/h5pp_impl.hpp
		$(CXX) -o $@ $< -Iinclude $(CXXFLAGS) -lhdf5 -shared

read_write_dataset:	examples/read_write_dataset.cpp
		$(CXX) -o $@ $< -Iinclude $(CXXFLAGS) -L. -lh5pp -lhdf5

extend_select_dataset:	examples/extend_select_dataset.cpp
		$(CXX) -o $@ $< -Iinclude $(CXXFLAGS) -L. -lh5pp -lhdf5
