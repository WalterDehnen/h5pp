/*
 *   Copyright (c), 2020, Walter Dehnen <wdehnen64@gmail.com>
 *
 *  Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE_1_0.txt or copy at
 *            http://www.boost.org/LICENSE_1_0.txt)
 *
 */
#ifndef h5pp_hpp
#  error  this file must be included only from h5pp.hpp
#endif

#include <numeric>

#ifndef h5pp_impl_hpp
#define h5pp_impl_hpp

//
namespace h5pp {

// size_table

inline bool operator==(size_table const&left, size_table const&rght)
{
    if(left.size() != rght.size()) {
	return false;
    }
    for(size_type dim=0; dim!=left.size(); ++dim)
	if( left[dim] != rght[dim] ) {
	    return false;
	}
    return true;
}

// Object

namespace details {

Object::Type type(id_type loc_id) noexcept;

} // namespace details

inline string name(Object::Type type)
{
    switch(type) {
#ifndef __clang__
    default:
#endif
    case Object::Type::Unknown: return "Unknown";
    case Object::Type::Invalid: return "Invalid";
    case Object::Type::File: return "File";
    case Object::Type::Group: return "Group";
    case Object::Type::UserDataType: return "DataType";
    case Object::Type::DataSpace: return "DataSpace";
    case Object::Type::DataSet: return "DataSet";
    case Object::Type::Attribute: return "Attribute";
    }
}

inline Object::Type Object::type() const noexcept
{
    return details::type(identifier);
}

inline Object& Object::operator=(Object &&other)
{
    reset_identifier(other.identifier);
    other.identifier = invalid_id;
    return *this;
}

// DataType

inline bool isFixedLengthString(DataType const&t)
{
    return t.getClass() == DataType::Class::String
	&& !isVariableLengthString(t);
}

inline bool compatible(DataType::Class a, DataType::Class b) noexcept
{
    return a==b ||
	(a==DataType::Class::Integer && b==DataType::Class::Float) ||
	(a==DataType::Class::Float && b==DataType::Class::Integer);
}

inline DataType::DataType(DataType const&other)
  : Object(copy(other.getId()))
{}

inline DataType& DataType::operator=(DataType const&other)
{
    if(other.getId() != getId()) {
	reset_identifier(copy(other.getId()));
    }
    return *this;
}

// DataSpace

// tested 22-01-2020
inline DataSpace::DataSpace(DataSpace const&other)
  : Object(copy(other.getId()))
  , num_selected(other.num_selected)
{}

// tested 22-01-2020
inline DataSpace& DataSpace::operator=(DataSpace const&other)
{
    if(other.getId() != getId()) {
	reset_identifier(copy(other.getId()));
	num_selected = other.num_selected;
    }
    return *this;
}

// DataSpace::Selection
namespace details {

inline size_type get_value(size_type dim,
    size_table const&table, size_type default_value)
{
    return dim < table.size() ? table[dim] : default_value;
}

inline size_type selection_count(size_type extent, size_type start,
    size_type count, size_type stride=1)
{
    if(start >= extent) {
	return 0;
    }
    auto max_count = 1 + (extent - start - 1) / stride;
    return std::min(count, max_count);
}

}

inline DataSpace::Selection::Selection(
    size_table strt, size_table cnt, size_table strd)
  : start(std::move(strt)), count(std::move(cnt)), stride(std::move(strd))
{
    for(size_type d=0; d!=stride.size(); ++d) {
	if(stride[d]==0) {
	    throw Exception("Selection: stride["+std::to_string(d)+"]=0");
	}
    }
}

inline DataSpace::Selection::Selection(
    size_type strt, size_type cnt, size_type strd)
  : start(1,strt), count(1,cnt), stride(1,strd)
{
    if(strd==0) {
	throw Exception("Selection: stride[0]=0");
    }
}

template<typename Func>
inline void DataSpace::Selection::loopthrough(size_table const&extent,
    Func&&func) const
{
    // avoid empty extent
    if(extent.size() == 0) {
	return;
    }
    // set rank, x[], b[], n[], s[]:
    // - ensure selected region is fully within extent
    // - combine dimensions with trivial selections with their next lower one
    //   in each dimension we loop i = b, b+s, ..., e=b+w
    size_table b(extent.size()), w(extent.size()), s(extent.size());
    size_table x(extent.size());
    size_type rank = 0;
    for(size_type d=0; d!=extent.size(); ++d) {
	x[rank] = extent[d];
	if(x[rank] == 1) {
	    // skip dimensions with extent=1
	    continue;
	}
 	b[rank] = details::get_value(d, start, 0);
	auto jn = details::get_value(d, count, x[rank]);
	s[rank] = details::get_value(d, stride, 1);
	jn      = details::selection_count(x[rank], b[rank], jn, s[rank]);
	if(jn == 0) {
	    // no elements selected!
	    return;
	} else if(rank && x[rank]==jn) {
	    // combine a fully selected dimension with the one earlier/lower
	    // (note: x==jn implies b==0, s==1, and jn==n)
	    x[rank-1] *= jn;
	    w[rank-1] *= jn;
	} else {
	    // cannot avoid this dimension
	    w[rank] = 1 + (jn-1) * s[rank];
	    rank++;
	}
    }
    // special case rank=0 (all dimensions have extent = 1)
    if(rank == 0) {
	func(0, 1, 1);
	return;
    }
    // special case rank=1 (only first/lowest dimension contains selection)
    if(rank == 1) {
	func(b[0], b[0]+w[0], s[0]);
	return;
    }
    // get to start: i[]=b[]
    size_type k=0;
    size_table i(rank);
    for(size_type d1=rank,inc=1; d1; --d1) {
	const auto d=d1-1;
	i[d] = b[d];
	k   += inc * i[d];
	inc *= x[d];
    }
    // go
    size_type last=rank-1;
    while( i[0] < b[0]+w[0] ) {
	// loop in last/highest dimension
	func(k, k+w[last], s[last]);
	k += x[last];
	// tick over earlier/lower dimensions
	for(size_type d1=last,inc=x[last]; d1; --d1) {
	    const auto d=d1-1;
	    // tick
	    i[d]+= s[d];                  // increment i[d]
	    k   += inc * (s[d]-1);        // increment k if stride > 1
	    if(i[d] < b[d]+w[d] || d==0)  // break for another tick at this d
		break;
	    // tickover:
	    i[d] = b[d];                  // reset i[d] back to start
	    k   += inc * (x[d]-w[d]);
	    inc *= x[d];
	}
    }
}

// Attribute

// tested 21-01-2020
template<typename Tp>
inline enable_if_t<is_basic_type<Tp>()>
Attribute::read(Tp&x) const
{
    if(size() != 1) {
	throw Exception("cannot read scalar from non-scalar attribute");
    }
    read(static_cast<void*>(&x), DataType::For<Tp>());
}

template<typename Tp>
inline enable_if_t<is_basic_type_or_string<Tp>(), Tp>
Attribute::read() const
{
    Tp x;
    read(x);
    return x;
}

// tested 21-01-2020
template<typename Container>
inline enable_if_t<is_resizeable_contiguous_container<Container>() &&
		   is_basic_type<typename Container::value_type>()>
Attribute::read(Container &container) const
{
    container.resize(size());
    read(static_cast<void*>(container.data()),
	DataType::For<typename Container::value_type>());
}

template<typename Container>
inline enable_if_t<is_fixed_size_contiguous_container<Container>() &&
		   is_basic_type<typename Container::value_type>()>
Attribute::read(Container &container) const
{
    if(container.size() != size()) {
	throw Exception("cannot write attribute of size " +
	    std::to_string(size()) + " into fixed-size container of size " +
	    std::to_string(container.size()));
    }
    read(static_cast<void*>(container.data()),
	DataType::For<typename Container::value_type>());
}

template<typename Container>
inline enable_if_t<is_contiguous_container<Container>() &&
		   is_basic_type<typename Container::value_type>(), Container>
Attribute::read() const
{
    Container x;
    read(x);
    return x;
}

// tested 21-01-2020
template<typename T>
inline enable_if_t<is_basic_type<T>()>
Attribute::write(const T&datum)
{
    if(size() != 1) {
	throw Exception("cannot write scalar to non-scalar attribute");
    }
    write(static_cast<const void*>(&datum), DataType::For<T>());
}

// tested 21-01-2020
inline void Attribute::write(const string&datum)
{
    if(size() != 1) {
	throw Exception("cannot write string to non-scalar attribute");
    }
    auto cstr = datum.c_str();
    write(static_cast<const void*>(&cstr), DataType::For<string>());
}

// tested 21-01-2020
template<typename Container>
inline enable_if_t<is_contiguous_container<Container>() &&
		   is_basic_type<typename Container::value_type>()>
Attribute::write(Container &container)
{
    if( size() != container.size() ) {
	throw Exception("writing 1D container to attribtue: expect "+
	    std::to_string(size())+" data, but "+
	    std::to_string(container.size())+" provided");
    }
    write(static_cast<const void*>(container.data()),
	DataType::For<typename Container::value_type>());
}

// Location

// tested 21-01-2020
template<typename Tp>
inline enable_if_t<is_basic_type<Tp>()>
Location::writeAttribute(std::string const&name, Tp const&datum)
{
    auto type = DataType::For<Tp>();
    auto attr = createAttribute(name,DataSpace::scalar(),type);
    attr.write(&datum,type);
}

// tested 21-01-2020
inline void Location::writeAttribute(std::string const&name, const char*cstr)
{
    auto type = DataType::For<string>();
    auto attr = createAttribute(name,DataSpace::scalar(),type);
    attr.write(&cstr,type);
}

// tested 21-01-2020
template<typename Container>
inline enable_if_t<is_contiguous_container<Container>() &&
		   is_basic_type<typename Container::value_type>()>
Location::writeAttribute(string const&name, Container const&container)
{
    auto type = DataType::For<typename Container::value_type>();
    auto attr = createAttribute(name,DataSpace(container.size()),type);
    attr.write(container.data(),type);
}

template<typename Tp>
inline enable_if_t<is_basic_type<Tp>()>
Location::updateAttribute(string const&name, Tp const&datum)
{
    auto type = DataType::For<Tp>();
    auto hasA = hasAttribute(name);
    auto attr = hasA?
	openAttribute(name) :
	createAttribute(name,DataSpace::scalar(),type);
    if(hasA) {
	if(attr.size() != 1) {
	    throw Exception("Location::updateAttribute '" + name +
		"': size mismatch (1 vs " + std::to_string(attr.size()) + ')');
	}
	if(!compatible(type,attr.dataType())) {
	    throw Exception("Location::updateAttribute '" + name +
		"': incompatible data types (" + type.description() + " vs "
		+ attr.dataType().description());
	}
    }
    attr.write(&datum,type);
}

inline void Location::updateAttribute(std::string const&name, const char*cstr)
{
    auto type = DataType::For<string>();
    auto hasA = hasAttribute(name);
    auto attr = hasA?
	openAttribute(name) :
	createAttribute(name,DataSpace::scalar(),type);
    if(hasA) {
	if(attr.size() != 1) {
	    throw Exception("Location::updateAttribute '" + name +
		"': size mismatch (1 vs " + std::to_string(attr.size()) + ')');
	}
	if(!compatible(type,attr.dataType())) {
	    throw Exception("Location::updateAttribute '" + name +
		"': incompatible data types (" + type.description() + " vs "
		+ attr.dataType().description());
	}
    }
    attr.write(&cstr,type);
}

template<typename Container>
inline enable_if_t<is_contiguous_container<Container>() &&
		   is_basic_type<typename Container::value_type>()>
Location::updateAttribute(string const&name, Container const&container)
{
    auto type = DataType::For<typename Container::value_type>();
    auto hasA = hasAttribute(name);
    auto attr = hasA?
	openAttribute(name) :
	createAttribute(name,DataSpace(container.size()),type);
    if(hasA) {
	if(attr.size() != container.size()) {
	    throw Exception("Location::updateAttribute '" + name +
		"': size mismatch (" + std::to_string(container.size()) +
		" vs " + std::to_string(attr.size()) + ')');
	}
	if(!compatible(type,attr.dataType())) {
	    throw Exception("Location::updateAttribute '" + name +
		"': incompatible data types (" + type.description() + " vs "
		+ attr.dataType().description());
	}
    }
    attr.write(container.data(),type);
}

// DataSet

template<typename Container>
inline enable_if_t<is_resizeable_contiguous_container<Container>() &&
		   is_basic_type_or_string<typename Container::value_type>()>
DataSet::read(Container&container, DataSpace::Selection const&fileSelect) const
{
    const auto ndata = fileSelect.numSelected(space());
    container.resize(ndata);
    read(container.data(), DataSpace(ndata), fileSelect);
}

template<typename Container>
inline enable_if_t<is_resizeable_contiguous_container<Container>() &&
		   is_basic_type_or_string<typename Container::value_type>()>
DataSet::read(Container&container) const
{
    const auto ndata = space().numElements();
    container.resize(ndata);
    read(container.data(), DataSpace(ndata));
}

// Group

inline Group::Group(id_type i, const Location*p, string const&n, bool created)
  : Location(i,p,n)
  , location_names(created? new loc_names : nullptr)
{}

inline Group::nameSet const&Group::dataSetNames() const
{
    if(!location_names)
	retrieve_location_names();
    return location_names->dst;
}

inline Group::nameSet const&Group::groupNames() const
{
    if(!location_names)
	retrieve_location_names();
    return location_names->grp;
}

inline DataSet Group::createDataSet(string const&name,
    DataSpace const&space, DataType const&type,
    DataSetCreateProperties const&createProps,
    DataSetAccessProperties const&accessProps)
{
    return {create_dataset_extendible(name,space.getId(),type.getId(),
		createProps.getId(),accessProps.getId()),
	    this, name};
}

template<typename Container>
inline enable_if_t<is_contiguous_container<Container>() &&
		   is_basic_type_or_string<typename Container::value_type>(),
		   DataSet >
Group::createDataSetFor(string const&name, Container const&container,
    DataSetCreateProperties const&createProps,
    DataSetAccessProperties const&accessProps)
{
    DataSpace space(container.size());
    const auto type = DataType::For<typename Container::value_type>();
    return {create_dataset_nonextendible(name, space.getId(), type.getId(),
		createProps.getId(), accessProps.getId()),
	    this, name};
}

template<typename Container>
inline enable_if_t<is_contiguous_container<Container>() &&
		   is_basic_type_or_string<typename Container::value_type>()>
Group::writeDataSet(string const&name, Container const&container,
    DataSetCreateProperties const&createProps,
    DataSetAccessProperties const&accessProps)
{
    createDataSetFor(name,container,createProps,accessProps).write(container);
}

template<typename Container>
inline enable_if_t<is_resizeable_contiguous_container<Container>() &&
		   is_basic_type_or_string<typename Container::value_type>()>
Group::readDataSet(string const&name, Container &container,
    DataSetAccessProperties const&accessProps)
{
    auto dset = openDataSet(name, accessProps);
    dset.read(container);
}

// File

inline void File::open(string const&file_name, unsigned open_flags,
    FileAccessProperties const&accessProps)
{
    reset_file(
	open(file_name, open_flags, accessProps.getId()),
	file_name,
	create_file(open_flags),
	false);
}



} // namespace h5pp
//
#endif  // h5pp_impl_hpp
