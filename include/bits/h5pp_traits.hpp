/*
 *  Copyright (c), 2020, Walter Dehnen <wdehnen64@gmail.com>
 *
 *  Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 *
 */
#ifndef h5pp_hpp
#  error  this file must be included only from h5pp.hpp
#endif

#include <type_traits>

namespace h5pp {

/// contains useful traits
namespace traits {

using std::true_type;
using std::false_type;
using std::is_same;
using std::is_void;
using std::declval;

#ifdef __clang__
#    pragma clang diagnostic push
// The meta-template magic here relies on '0' being converted to void* if the
// int version fails. Hence we switch off the corresponding clang warning here.
#    pragma clang diagnostic ignored "-Wzero-as-null-pointer-constant"
#    pragma clang diagnostic ignored "-Wunused-template"
#endif

/// contains some implementation details
namespace tests {

template<typename>
struct sfinae_true : true_type {};

// testing for Tp::value_type
template<typename Tp>
static auto value_type(int) -> sfinae_true<typename Tp::value_type>;
template<typename Tp>
static auto value_type(void*) -> false_type;

// testing for Tp::data()
template<typename Tp, typename Result>
static auto data(int)
  -> sfinae_true<enable_if_t<is_same<decltype(declval<Tp>().data()),
				     Result>::value> >;
template<typename Tp, typename Result>
static auto data(void*) -> false_type;

// testing for Tp::size() to exist and return S
template<typename Tp, typename S>
static auto size(int)
  -> sfinae_true<enable_if_t<is_same<decltype(declval<Tp>().size()),S>::value>>;
template<typename Tp, typename S>
static auto size(void*) -> false_type;

// testing for Tp::resize() to exist and take size_t
template<typename Tp>
static auto resize(int) -> sfinae_true
    < enable_if_t
      < is_void<decltype(declval<Tp>().resize(size_t(0)))>::value > >;
template<typename Tp>
static auto resize(void*) -> false_type;

} // namespace h5pp::traits::tests

struct any_type {};
/// does sub-type Container::value_type exist and equals some_type
template<typename Container, typename some_type = any_type,
	 bool = decltype(tests::value_type<Container>(0))::value >
struct has_value_type
  : false_type {};

template<typename Container, typename some_type>
struct has_value_type<Container,some_type,true>
  : std::is_same<typename Container::value_type, some_type> {};

template<typename Container>
struct has_value_type<Container,any_type,true>
  : true_type {};

/// does Container::data() exist and return Container::value_type* ?
template<typename Container, bool = has_value_type<Container>::value>
struct has_data : false_type {};
template<typename Container>
struct has_data<Container, true>
  : decltype(tests::data<Container,typename Container::value_type*>(0)) {};

/// does Container::size() exist and return size_t ?
template<typename Container, typename S=size_t>
using has_size =
    decltype(tests::size<Container,S>(0));

/// does Container::resize(size_t) exist?
template<typename Container>
using has_resize =
    decltype(tests::resize<Container>(0));



#ifdef __clang__
#    pragma clang diagnostic pop
#endif

} // namespace h5pp::traits

} // namespace h5pp
