/*
 *   Copyright (c), 2020, Walter Dehnen <wdehnen64@gmail.com>
 *
 *  Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE_1_0.txt or copy at
 *            http://www.boost.org/LICENSE_1_0.txt)
 *
 */
#ifndef h5pp_hpp
#define h5pp_hpp

#include <ctime>
#include <vector>
#include <limits>
#include <string>
#include <memory>
#include <complex>
#include <cstdint>
#include <type_traits>
#include <unordered_set>

#ifdef __clang__
//  switch off certain clang warning messages
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wc++98-compat-pedantic"
#  pragma clang diagnostic ignored "-Wpadded"
#  pragma clang diagnostic ignored "-Wdocumentation"
#endif

/**

   h5pp project

   idea: a simple portable C++ interface for HDF5
   
   why not the official HDF5 C++ interface? 
   That interface is written in a confusing non-portable way, overly
   complicated, poorly documented, not threadsafe, and incomplete w/o ability
   to extend

   why not other C++ interfaces?
   These don't seem to be well supported, incomplete, or faulty.
   HighFive, for instance, appears to have stopped development 2017 (uses
   deprecated HDF5 features), leaks, crashes (File::numAttributes), and does
   not support low-level I/O.

   design principles:
   -- C++11 standard or higher
   -- standalone: dependent only on C++ standard library and HDF5's C API 
   -- hide HDF5 C API from user, avoid deprecated HDF5 features (version 1.10)
   -- avoid bloated templates and headers, pre-compile as much as possible
   -- provide some basic functionality with the option for extensions
   -- full interface defined here, inline methods in bits/h5pp_impl.hpp   

   immediate goals
   -- implement file, group, data set, data types, attributes, filters

 */

namespace h5pp {

/*

  class structure

  Object

  DataType final : Object
  DataSpace final : Object
  DataSpace::Selection

  Property<Type>
  PropertyList<Type>

  Chunked : Property<datasetCreate>
  Shuffle : Property<datasetCreate>
  CompressGzip : Property<datasetCreate>
  Fletcher32 : Property<datasetCreate>
  Caching : Property<fileAccess>, Property<datasetAccess>
 
  Attribute : Object
  Location : Object
  DataSet : Location
  Group : Location
  File : Group

 */

using std::string;
using id_type = std::int64_t;
using time_type = std::time_t;
using size_type = std::size_t;
using hsize_type = unsigned long long;
using fileaddress_type = std::uint64_t;
using size_table = std::vector<size_type>;

inline bool operator==(size_table const&, size_table const&);

#if __cplusplus >= 201402L

using std::enable_if_t;
using std::conditional_t;

#else

template<bool C, typename T=void>
using enable_if_t = typename std::enable_if<C,T>::type;

template<bool C, typename A, typename B>
using conditional_t = typename std::conditional<C,A,B>::type;

#endif

/// is Tp a native type (bool, integer, floating-point) or std::complex?
template<typename Tp>
inline constexpr bool is_basic_type()
{
    return
	std::is_arithmetic<Tp>::value ||
	std::is_same<Tp,bool>::value ||
	std::is_same<Tp,std::complex<double>>::value ||
	std::is_same<Tp,std::complex<float>>::value;
}

// is Tp a basic type or a string?
template<typename Tp>
inline constexpr bool is_basic_type_or_string()
{
    return is_basic_type<Tp>() || std::is_same<Tp,string>::value;
}

} // namespace h5pp
//
#include "bits/h5pp_traits.hpp"
//
namespace h5pp {

/// a contiguous container must have:
///   nested type   value_type
///   method        value_type* data(); const value_type* data() const;
///   method        size_t size() const;
template<typename Container>
inline constexpr bool is_contiguous_container() noexcept
{
    return
	traits::has_data<Container>::value &&
	traits::has_size<Container>::value;
}

/// a fixed-size contiguous container must have:
///   nested type   value_type
///   method        value_type* data(); const value_type* data() const;
///   method        size_t size() const;
/// but must not have
///   method        void resize();

template<typename Container>
inline constexpr bool is_fixed_size_contiguous_container() noexcept
{
    return
	is_contiguous_container<Container>() &&
	! traits::has_resize<Container>::value;
}

/// a resizeable contiguous container must have:
///   nested type   value_type
///   method        value_type* data(); const value_type* data() const;
///   method        size_t size() const;
///   method        void resize();
template<typename Container>
inline constexpr bool is_resizeable_contiguous_container() noexcept
{
    return
	is_contiguous_container<Container>() &&
	traits::has_resize<Container>::value;
}

/// an extent_type must have:
///   nested type   value_type == size_type
///   method        value_type* data(); const value_type* data() const
///   method        size_t size() const;

template<typename ExtentType>
inline constexpr bool is_extent_type() noexcept
{
    return
        is_contiguous_container<ExtentType>() &&
	traits::has_value_type<ExtentType,size_type>::value;
}

struct Object;
struct DataType;
struct DataSpace;
struct Attribute;
struct Location;
struct DataSet;
struct Group;
struct File;

///
/// exceptions used by h5pp
///
/// \details h5pp will throw only h5pp::Exception to report inconsistent
///          input (if detectable) and errors raised by the HDF5 library (when
///          the full error stack will be reported too). Almost all calls to
///          the HDF5 library are guarded to suppress direct error reporting
///          from the HDF5 library, but these guards are lifted after each
///          call, so that other users of the HDF5 library should not see a
///          change in the error reporting.
struct Exception final
  : std::runtime_error
{
    Exception(std::string const&error)
      : std::runtime_error("h5pp: " + error) {}
  private:
    // implemented in h5pp.cpp to place the v-table there (and avoid a clang
    // warning about weak-vtable).
    virtual void dummy() const;
};

///
/// set error reporting depth
/// \details default is 1, when the HDF5 error stack is reported. Currently,
///          the only alternative is 0, when the HDF5 error stack is not
///          reported.
void setErrorReportingDepth(unsigned depth);

///
/// set debugging reporting depth
/// \details Debug reporting is for the benefit of the developer(s). The
///          default reporting depth is 0, when no reporting is done.
void setDebugReportingDepth(unsigned depth);

///
/// base class for all h5pp objects that represent HDF5 objects
///
/// \details An Object holds a HDF5 object id, which is really a link to an
///          actual object. The actual object is properly destroyed (equivalent
///          to a C++ virtual destructor) only with the last Object holding a
///          link to it.
///          The resources managed by most HDF5 objects, such as file segments
///          and memory buffers, are shared, and in C++ would most likely be
///          implemented using a shared_ptr<>. In the HDF5 C API, they are
///          implemented in a similar way with a reference counter, which when
///          it reaches zero triggers closing (destruction) of the object.
struct Object
{
    /// enum for the types of objects
    enum struct Type
    {
	 File,
	 Group,
	 DataSpace,
	 DataSet,
	 Attribute,
	 UserDataType,   //  currently not used in h5pp
	 Unknown,        //  valid, but not used in h5pp
	 Invalid
    };

    // identical to H5I_INVALID_HID
    static constexpr id_type invalid_id = id_type(-1);
    
    /// decrease reference counter
    /// \note If the reference count drops to zero, the HDF5 library also
    ///       closes the object. Hence, no further calls to the HDF5 library,
    ///       like H5Sclose(), are required in destructors of derived classes.
    ~Object()
    { reset_identifier(invalid_id, true); }

    /// default constructor: an empty Object
    Object() = default;

    /// move constructor, avoids calls to HDF5 library
    Object(Object &&other) noexcept : identifier(other.identifier)
    { other.identifier = invalid_id; }
    
    /// move assignment invalidates argument
    Object&operator=(Object &&);
      
    /// is the current object a valid HDF5 object?
    bool valid() const noexcept;
    bool isValid() const noexcept
    { return valid(); }

    /// Object type
    Type type() const noexcept;
    Type getType() const noexcept
    { return type(); }

    /// HDF5 identifier
    id_type getId() const noexcept
    { return identifier; }
  protected:
    explicit Object(id_type id) noexcept
      : identifier(id) {}
    void reset_identifier(id_type, bool nothrow=false);
  private:
    id_type identifier = invalid_id;
};  // struct Object

/// printable type name
/// \relates Object::Type
inline string name(Object::Type);

///
/// a HDF5 data type
///
/// \note  presently, we only allow for basic data types
/// \todo  support compound and array types
///
struct DataType final
  : Object
{
    /// enum of fundamental data classes
    /// \details HDF5 read/write operation convert between float and double
    ///          (class Float), or any integers, any enums, any string types
    ///          (truncating if required)
    enum struct Class
    {
        Time,
	Integer,
	Float,
	String,
	BitField,
	Opaque,
	Compound,
	Reference,
	Enum,
	VarLen,
	Array,
	Invalid
    };

    /// name of data class
    static string name(Class) noexcept;
    
    /// default constructor: invalid
    DataType() = default;

    /// move construction and assignment invalidate their argument
    DataType(DataType&&) = default;
    DataType&operator=(DataType&&) = default;

    /// copy construction and assignment
    DataType(DataType const&);
    DataType&operator=(DataType const&other);

    /// comparison
    bool operator==(DataType const&) const;
    bool operator!=(DataType const&other) const
    { return !(operator==(other)); }

    /// obtain fundamental type class
    /// \note Class::Invalid is returned also in case of an HDF5 error
    Class getClass() const noexcept;

    /// is the data type a signed integer type?
    bool isSigned() const noexcept;
    
    /// is the data type an unsigned integer type?
    bool isUnsigned() const noexcept;

    /// size of data type in bytes
    /// \note   this is misleading for variable length objects, such as string
    size_type size() const;
    size_type getSize() const
    { return size(); }

    /// type descriptions, like 'Float32'
    string description() const;
    string getDescription() const
    { return description(); }

    /// data type for all arithmetic types, complex, bool, and string
    ///
    /// \param[in] utf8   for Tp==string, use utf-8 (instead of ASCII) encoding?
    ///
    /// \note  Conversion between different encoding is not supported by h5pp.
    template<typename Tp>
    static enable_if_t<is_basic_type_or_string<Tp>(), DataType>
    For(bool utf8=false) noexcept(is_basic_type<Tp>())
    { return typeFor<Tp>(utf8); }

  private:
    template<typename Tp>
    static DataType typeFor(bool=false) noexcept(is_basic_type<Tp>());
    friend struct Attribute;
    friend struct DataSet;
    DataType(id_type id) noexcept
      : Object(id) {}
    static id_type copy(id_type);
};  // struct DataType

/// are DataType::Classes compatible?
/// \return true if a==b or if either is Integer and the other Float
/// \related DataType
bool compatible(DataType::Class a, DataType::Class b) noexcept;

/// are DataTypes compatible?
/// \return true if a==b or if either is Integer and the other Float
/// \related DataType
inline bool compatible(DataType const&a, DataType const&b)
{ return compatible(a.getClass(), b.getClass()); }

/// is a data type a variable-length string type?
/// \throws Exception if H5Tis_variable_str() reports an error
bool isVariableLengthString(DataType const&);

/// is a data type a fixed-length string?
/// \throws Exception if H5Tis_variable_str() reports an error
bool isFixedLengthString(DataType const&);

/// is a data type a utf-8 encoded string?
bool isUTF8EncodedString(DataType const&);

/// is a data type an ASCII encoded string?
/// \throws Exception if H5Tis_variable_str() reports an error
bool isASCIIEncodedString(DataType const&);

///
/// a N-dimensional cuboidal area representing data layout
/// 
/// \details A data space can be 'null', 'scalar' and 'simple'. A 'null' space
///          has no elements; a 'scalar' space has one element, and a 'simple'
///          space is cuboidal with rank>0, but may only contain one element.
///
/// \note    If we made a cuboidal data space with one element 'scalar', its
///          rank is truncated to 0. This may cause unexpected trouble, for
///          example the chunks for a dataset-creation property-list must have
///          the same rank as the data space.
///
struct DataSpace final
  : Object
{
    /// specify extendibility of data set created with a dataspace
    enum Extend {
	Not,            ///< not extendible
	FirstDim,       ///< unlimited extendible in the lowest dimension
	AllDims         ///< unlimited extendible in the all dimensions
    };
    /// use extent[dim] = unlimited for unlimited maximum extent
    static constexpr size_type unlimited= std::numeric_limits<size_type>::max();
    
    /// make a null data space
    static DataSpace null() noexcept
    { return DataSpace(make_null(),size_type(0)); }

    /// make a scalar data space
    static DataSpace scalar() noexcept
    { return DataSpace(make_scalar(),size_type(1)); }

    /// make a simple (or null) data space even for only one element
    /// \param[in] extent      (initial) extent(s)
    /// \param[in] extendible  indicate extendibility
    /// \note if extent.size()==0, a null space is created
    static DataSpace simple(size_type extent, Extend extendible=Extend::Not)
    { return DataSpace(make_simple(1,&extent,extendible),extent); }

    template<typename Extent>
    static enable_if_t<is_extent_type<Extent>(), DataSpace>
    simple(Extent const&extent, Extend extendible=Extend::Not)
    { return DataSpace(make_simple(extent.size(),extent.data(),extendible)); }

    static DataSpace simple(std::initializer_list<size_type>&&extent,
	Extend extendible=Extend::Not)
    { return simple(size_table{extent.begin(),extent.end()}, extendible); }

    /// make a simple extendible (or null) data space
    ///
    /// \param[in] initialExtent  dimensionality and initial extents    
    /// \param[in] maximumExtent  maximial extent to which the space can be
    ///            extended. May have smaller rank than iniExtent, when the
    ///            latter is used (i.e. no extensions possible in such
    ///            dimensions). Entries can be DataSpace::unlimited
    /// \note if initialExtent.size()==0, a null space is created
    template<typename iExtent, typename mExtent>
    static enable_if_t<is_extent_type<iExtent>() && is_extent_type<mExtent>(),
		       DataSpace>
    simple(iExtent const&initialExtent, mExtent const&maximumExtent)
    {
	return DataSpace(
	    make_simple(initialExtent.size(),initialExtent.data(),
		        maximumExtent.size(),maximumExtent.data()));
    }
    
    static DataSpace simple(std::initializer_list<size_type>&&initialExtent,
	                    std::initializer_list<size_type>&&maximumExtent)
    { return simple(size_table{initialExtent.begin(),initialExtent.end()},
	            size_table{maximumExtent.begin(),maximumExtent.end()}); }
		    
    /// move constructor
    DataSpace(DataSpace &&) = default;

    /// move assignment
    DataSpace&operator=(DataSpace &&) = default;
    
    /// copy constructor: create an identical HDF5 object with new HDF5 id
    DataSpace(DataSpace const&other);

    /// copy assignment: create an identical HDF5 object with new HDF5 id
    DataSpace& operator=(DataSpace const&other);

    /// construct a 1D data space of n=extent objects
    /// \param extent       (initial) extent
    /// \note for extent==0 a null data space is created
    /// \note for extent==1 a scalar data space is created
    /// \note for extent >1 a simple data space is created
    /// \param extendible   only used if extent > 1.
    DataSpace(const size_type extent=0, Extend extendible=Extend::Not)
      : DataSpace(extent==0? make_null() : extent==1? make_scalar() :
	  make_simple(1,&extent,extendible), extent) {}

    /// construct a simple (or null) data space with given extent
    /// \param[in] extent      dimensionality and (initial) extents
    /// \param[in] extendible  indicate extendibility
    /// \note if extent.size()==0, a null space is created
    template<typename Extent>
    explicit DataSpace(Extent const&extent, Extend extendible=Extend::Not,
	enable_if_t<is_extent_type<Extent>()>* =nullptr)
      : DataSpace(simple(extent,extendible)) {}

    DataSpace(std::initializer_list<size_type>&&extent,
	Extend extendible=Extend::Not)
      : DataSpace(simple(std::move(extent),extendible)) {}

    /// construct a resizable simple (or null) data space
    ///
    /// \param[in] initialExtent  dimensionality and initial extents    
    /// \param[in] maximumExtent  maximial extent to which the space can be
    ///            extended. May have smaller rank than iniExtent, when the
    ///            latter is used (i.e. no extensions possible in such
    ///            dimensions). Entries can be DataSpace::unlimited
    /// \note if initialExtent.size()==0, a null space is created
    template<typename iExtent, typename mExtent>
    explicit DataSpace(iExtent const&initialExtent, mExtent const&maximumExtent,
	enable_if_t<is_extent_type<iExtent>() &&
	            is_extent_type<mExtent>() >* =nullptr)
      : DataSpace(simple(initialExtent,maximumExtent)) {}

    DataSpace(std::initializer_list<size_type>&&initialExtent,
	      std::initializer_list<size_type>&&maximumExtent)
      : DataSpace(simple(std::move(initialExtent),std::move(maximumExtent))) {}
    
    /// rank = number of dimensions
    size_type rank() const { return numDimensions(); }
    size_type numDimensions() const;
    size_type getNumDimensions() const { return numDimensions(); }

    /// extent
    size_table extent() const;
    size_table getExtent() const { return extent(); }

    /// maximum extent, which may be unlimited
    size_table maxExtent() const;
    size_table getMaxExtent() const { return maxExtent(); }

    /// extent and maximum extent
    void getExtent(size_table&extent, size_table&maximumExtent) const;

    /// is this data space scalar (one element)?
    bool isScalar() const;

    /// is this data space extendible (in any dimension)?
    bool isExtendible() const;
    bool isExtensible() const
    { return isExtendible(); }

    /// \name select sub-set of data space
    //@{

    /// a cuboidal sub-region of a data space, possibly with stride
    ///
    /// \details data transfer can be restricted to cuboidal sub-regions of
    ///          both the memory and the file data spaces. The selection is
    ///          specified by start, count and stride, when in each dimension
    ///          the selected subset is constructed as
    ///          \code
    ///          for(j=start,i=k=0;  k<count;  k++, j+=stride)
    ///              if(j < extent)
    ///                  subset[i++] = set[j]
    ///          \endcode
    ///          Beyond the end of the specified start[], count[], stride[],
    ///          i.e. if the data space has higher rank than these tables, we
    ///          assume start=0, count=inf, stride=1 (selecting all).
    ///     
    struct Selection
    {
	const size_table start, count, stride;
	/// default constructor: select all
	Selection() = default;
	/// construction of strided cuboidal region
	/// \throws Exception  if stride == 0 for any dimension
	Selection(size_table strt, size_table cnt, size_table strd);
	/// construction of compact cuboidal region
	Selection(size_table strt, size_table cnt) noexcept
	  : start(std::move(strt)), count(std::move(cnt)) {}
	/// construction of strided cuboidal region; select only in 1st dim
	/// \throws Exception  if stride == 0
	Selection(size_type strt, size_type cnt, size_type strd);
	/// construction of compact cuboidal region; select only in 1st dim
	Selection(size_type strt, size_type cnt) noexcept
	  : start(1,strt), count(1,cnt) {}
	/// number of selected elements when applied to a data-space extent
	size_type numSelected(size_table const&extent) const noexcept;
	/// number of selected elements when applied to a data-space
	size_type numSelected(DataSpace const&space) const;
	/// is this the trivial selection (always all elements)
	bool trivial() const
	{ return start.empty() && count.empty() && stride.empty(); }
	/// loop over selection
	/// \param[in] loopRange  function implementing loop over sub-range.
	///                       loopRange(begin,end,stride) should execute
	///                       the loop k = begin, begin+stride, ..., end
	template<typename Func>
	void loopthrough(size_table const&extent, Func&&loopRange) const;
	/// loop over selection
	/// calls func(k) for all selected indices into data buffer
	///
	/// \note Implemented via loopthrough(), which deals with the selection
	///       and leaves plain loops to the user. A more efficient
	///       implementation of what you intend func(k) to achieve may be
	///       possible via loopthrough(); see code here.
	template<typename Func>
	void loop(size_table const&extent, Func&&func) const
	{
	    loopthrough(extent,[&](size_type k, size_type end, size_type str) {
				  for(; k<end; k+=str) func(k); });
	}
    };

    /// set a selection of the current data space for data transfer
    void select(Selection const&);
    void apply(Selection const&selection)
    { select(selection); }

    /// remove selection
    void resetSelection();
    
    /// is a selection in place?
    bool hasSelection() const;

    /// number of selected elements (after selection has been applied)
    size_type numSelected() const
    { return num_selected; }

    /// number of all elements w/o selection
    size_type numElements() const
    { return hasSelection()? totalcount(getId()): numSelected(); }

    //@}

  private:
    friend struct DataSet;
    DataSpace(id_type id, size_type nE=unlimited)
      : Object(id), num_selected(nE==unlimited? totalcount(id) : nE) {}
    static id_type make_null() noexcept;
    static id_type make_scalar() noexcept;
    static id_type make_simple(size_type, const size_type*,	
	size_type, const size_type*, Extend=Extend::Not);
    static id_type make_simple(size_type n, const size_type*ex, Extend exd)
    { return make_simple(n,ex,0,nullptr,exd); }	  
    static id_type copy(id_type);
    static size_type totalcount(id_type);
    size_type num_selected;
};  // struct DataSpace

/// do two data spaces have identical extents?
/// \relates DataSpace
bool haveIdenticalExtent(DataSpace const&, DataSpace const&);

///
/// enum: types of properties and property lists
///
/// \note  properties can be of multiple types
enum struct propertyType : int
{
     fileAccess     = 0
    ,datasetCreate  = 1
    ,datasetAccess  = 2
     /* these are currently not used by h5pp or even HDF5
	,groupCreate
	,objectCreate
	,fileCreate
	,dataTransfer
	,groupAccess
	,datatypeCreate
	,datatypeAccess
	,stringCreate
	,attributeCreate
	,objectCopy
	,linkCreate
	,linkAccess
     */
};

template<propertyType pT>
using relatedObject =
    conditional_t<pT==propertyType::fileAccess, File,
    conditional_t<pT==propertyType::datasetCreate ||
		  pT==propertyType::datasetAccess, DataSet, void > >;

/// name of property type
string name(propertyType type);

template<propertyType> struct PropertyList;

namespace details {

id_type default_property_id();

struct propertyBase
{
    virtual ~propertyBase();
    propertyBase() = default;
    propertyBase(propertyBase const&) = default;
  protected:
    virtual void apply(id_type, propertyType) const = 0;
    template<propertyType pType>
    static id_type getPropId(PropertyList<pType> const&);
};

} // namespace h5pp::details

///
/// specific property base class
///
template<propertyType pType>
struct Property
  : virtual details::propertyBase
{
  private:
    friend struct PropertyList<pType>;
    void operator() (id_type id) const
    { apply(id,pType); }
};
		    
///
/// A list of properties for the purpose indicated by the template parameter
/// 
template<propertyType T>
struct PropertyList final
{
    /// associated propertyType
    static constexpr propertyType type = T;

    /// associated property type
    using property = Property<type>;

    /// destructor
    ~PropertyList();

    /// default constructor creates an empty property list
    PropertyList();

    /// copy constructor
    PropertyList(PropertyList const&);

    /// copy assignment
    PropertyList&operator=(PropertyList const&);

    /// move constructor
    PropertyList(PropertyList &&);

    /// move assignent
    PropertyList&operator=(PropertyList &&);

    /// add one or more properties (use chaining to add more)
    PropertyList&add(property const&prop);

    // 
    id_type getId() const
    { return identifier; }
  private:
    id_type identifier = details::default_property_id();
    PropertyList(id_type);
    friend relatedObject<type>;
    friend struct details::propertyBase;
};  // struct PropertyList<>

/// name of property list of type
string propertiesName(propertyType type);

//
// only the following property lists are currently supported for client code
//
using FileAccessProperties = PropertyList<propertyType::fileAccess>;
using DataSetCreateProperties = PropertyList<propertyType::datasetCreate>;
using DataSetAccessProperties = PropertyList<propertyType::datasetAccess>;

//
//  currently, we support the following properties
//
//  name           propertyType
//  --------------------------------------------
//  Chunked        datasetCreate
//  Shuffle        datasetCreate
//  CompressGzip   datasetCreate
//  Fletcher32     datasetCreate
//  Caching        fileAccess, datasetAccess
//

///
/// set the layout of a dataSet to chunked and also the chunk size
/// \note chunked layout is required for various filters
///
struct Chunked final
  : Property<propertyType::datasetCreate>
{
    /// chunk sizes (number of elements, not bytes)
    const size_table chunks;

    /// number of dimensions
    /// \details on application, this must be 0 or match the DataSpace::rank()
    size_type rank() const
    { return chunks.size(); }

    //  no default constructor
    Chunked() = delete;
    
    //  no copy (there is no point as Chunked cannot be altered)
    Chunked(Chunked const&) = delete;
    
    /// construct from list of chunk sizes
    /// \note the rank of the chunk sizes must match that of the dataset
    Chunked(size_table c)
      : chunks(std::move(c)) { check(); }
    Chunked(std::initializer_list<size_type> c)
      : chunks(c.begin(),c.end()) { check(); }
    template<typename...Args>
    Chunked(Args... args)
      : chunks{static_cast<size_type>(args)...} { check(); }

    /// do DataSetCreateProperties specify a chunked layout
    static bool has(DataSetCreateProperties const&properties);

    /// obtain chunk sizes from DataSetCreateProperties with chunked layout
    static size_table getChunks(DataSetCreateProperties const&properties);

  private:
    void apply(id_type, propertyType) const override;
    void check() const;
};  // struct Chunked

///
/// property: shuffle filter
///
/// \details The shuffle filter de-interlaces a block of data by reordering
///          the bytes. The primary value lies in its coordinated use with a
///          compression filter; it does not provide data compression when
///          used alone. When the shuffle filter is applied to a dataset
///          immediately prior to the use of a compression filter, the
///          compression ratio achieved is often superior to that achieved by
///          the use of a compression filter without the shuffle filter.
///
struct Shuffle final
  : Property<propertyType::datasetCreate>
{
    /// is shuffling currently supported by HDF5?
    static bool available();

    /// do properties contain the 'shuffle' property?
    static bool has(DataSetCreateProperties const&properties);

  private:
    void apply(id_type, propertyType) const override;
};  // struct Shuffle

///
/// property: compression using gzip algorithm
///
struct CompressGzip final
  : Property<propertyType::datasetCreate>
{
    /// is CompressGzip currently supported by HDF5?
    static bool available();
    /// deflation level
    /// \details a value of 0 implies no compression and 9 maximum compression.
    ///          The default of level=1 gives high speed and good compression
    ///          on trivially compressible data (such as all values identical)
    const unsigned level;
    CompressGzip(unsigned l=1) noexcept
      : level(l) {}
    /// do properties contain the gzip compression?
    static bool has(DataSetCreateProperties const&props);
    /// obtain compression level in a given set of properties
    static unsigned getLevel(DataSetCreateProperties const&props);
  private:
    void apply(id_type, propertyType) const override;
};  // struct CompressGzip

///
/// property: the Fletcher32 checksum filter for error detection
///
struct Fletcher32 final
  : Property<propertyType::datasetCreate>
{
    /// do properties contain the Fletcher32 checksum?
    static bool has(DataSetCreateProperties const&props);
  private:
    void apply(id_type, propertyType) const override;
};

///
/// property: specify non-default cache parameters at the file or dataset level
///
struct Caching final
  : Property<propertyType::fileAccess>
  , Property<propertyType::datasetAccess>
{
    /// total size in bytes of the raw data chunk cache for the dataset
    /// \details Larger numbers improve performance, if you have enough free
    ///          memory. A value of 0 maps to the defaults, which are 1MB for
    ///          files and the value from the FileAccessProperties for datasets
    size_type cacheSize = 0;

    /// number of chunk slots in the raw data chunk cache for the dataset
    /// \details Larger values reduce cache collisions, but slightly increase
    ///          the memory used. Due to the hashing strategy, this should be a
    ///          prime number. As a rule of thumb, it should be at least 10
    ///          times the number of chunks that can fit in cacheSize, but 100
    ///          times for maximum performance.  A value 0 maps to the
    ///          defaults, which is 521 for files and the value from the
    ///          FileAccessProperties for datasets
    size_type numSlots = 0;

    /// chunk preemption policy for this dataset
    /// \details Must be between 0 and 1 inclusive and indicates the weighting
    ///          according to which chunks which have been fully read or
    ///          written are penalized when determining which chunks to flush
    ///          from cache. A value of 0 means fully read or written chunks
    ///          are treated no differently than other chunks (the preemption
    ///          is strictly LRU) while a value of 1 means fully read or
    ///          written chunks are always preempted before other chunks. If
    ///          your application only reads or writes data once, this can be
    ///          safely set to 1. Otherwise, this should be set lower,
    ///          depending on how often you re-read or re-write the same data.
    ///          A value < 0 or > 1 maps to the defaults, which is 0.75 for
    ///          files and the value from the fileaccessPropertieList for
    ///          datasets
    double preemptionPolicy = 0.75;

    /// obtain chache parameters for a set of properties
    template<propertyType pType>
    static enable_if_t<pType==propertyType::datasetAccess ||
		       pType==propertyType::fileAccess, Caching>
    get(PropertyList<pType> const&properties)
    { return get(getPropId(properties),pType); }

  private:
    void apply(id_type, propertyType) const override;
    static Caching get(id_type, propertyType);
};  // struct Caching

///
/// a light-weight dataset for scalar or 1D data sets of basic types or strings
///
/// \note  Often, reading and writing attributes can be achieved without ever
///        dealing with this struct, via the short-cut interface at Location,
///        providing functions such as readAttribute() and writeAttribute().
///
struct Attribute final
  : Object
{
    /// name of the attribute
    string name() const;
    string localName() const
    { return name(); }
    string getName() const
    { return name(); }
    string getLocalName() const
    { return name(); }

    /// storage size of an attribute
    size_type storageSize() const;
    size_type getStorageSize() const
    { return storageSize(); }
    
    /// associated datatype as on disc
    DataType dataType() const;
    DataType getDataType() const
    { return dataType(); }

    /// size of associated data space
    /// \note an Attribute has the same data space on disc and memory
    size_type size() const;
    size_table extent() const
    { return size_table({size()}); }

    /// low-level read interface (with almost no error checking)
    /// \details reads the Attribute into buffer, which must hold enough
    ///          memory. If the memory DataType of the buffer is not provided,
    ///          we assume the same as on disc.
    /// \note    warning: only use if you know the C API of HDF5
    /// \throws  if the data types don't match
    void read(void*buffer, DataType const&memoryDataType={}) const;

    /// read a single datum of basic type or string
    /// \throws  if !space().isScalar() and data types don't match
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()> read(Tp&) const;
    void read(string&) const;

    /// read and return a single basic type or string
    template<typename Tp>
    enable_if_t<is_basic_type_or_string<Tp>(),Tp> read() const;

    /// read into a 1D vector/array of basic type
    /// \throws if data space on disc is not 1D and data types don't match
    template<typename Container>
    enable_if_t<is_resizeable_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    read(Container &) const;
    template<typename Container>
    enable_if_t<is_fixed_size_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    read(Container &) const;
    /// read and return a 1D vector/array of basic type
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>(), Container>
    read() const;
      
    /// low-level write interface (with almost no error checking)
    /// \details writes the buffer, which must hold enough memory, to the
    ///          Attribute on disc. If the memory DataType of the buffer is
    ///          not provided, we assume the same as on disc.
    /// \note    warning: only use if you know the C API of HDF5
    /// \throws  if the data types don't match
    void write(const void*buffer, DataType const&memoryDataType={});

    /// write a single string
    void write(string const&);

    /// write a single datum of basic type
    /// \throws  if !space().isScalar() and data types don't match
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    write(const Tp&);

    /// write a 1D array of data of basic type
    /// \tparam Container  must have basic valuetype, size(), and data()
    /// \throws if data spaces don't match or data types don't match
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    write(Container &);

  private:
    Attribute(id_type id)
      : Object(id) {}
    friend struct Location;
};  // struct Attribute

///
/// base class for DataSet and Group (and hence File)
///
struct Location
  : Object
{
    /// name of this inside parent (given at creation), empty for root (file)
    string localName() const
    { return local_name; }
    string getLocalName() const
    { return localName(); }

    /// full name within file, i.e. "directory/file.h5/group1/group2/datasetA"
    string fullName() const
    { return full_name; } 
    string getFullName() const
    { return fullName(); }

    /// name of file to which this location belongs
    string fileName() const;
    string getFileName() const
    { return fileName(); }

    ///
    /// some location info
    ///
    struct Info
    {
	const fileaddress_type fileAddress;   ///< address within its file
	const size_type refCount;             ///< # references to object
	const time_type creationTime;         ///< creation time
	const time_type accessTime;           ///< last access time
	const time_type modificationTime;     ///< last modification time
	const size_type numAttributes;        ///< # attributes
    };

    /// file address, reference count, creation and modification time
    Info info() const;
    Info getInfo() const
    { return info(); }

    /// retrieve names from all attributes in alphabetical order
    std::vector<string> attributeNames() const;
    std::vector<string> listAttributeNames() const
    { return attributeNames(); }   
    std::vector<string> getListAttributeNames() const
    { return attributeNames(); }

    /// retrieve the total number of attributes
    size_type numAttributes() const
    { return info().numAttributes; }
    size_type getNumAttributes() const
    { return numAttributes(); }

    /// does an attribute of given name exist?
    bool hasAttribute(string const&name) const;
    
    /// retrieve attribute of given name
    /// \throws if no attribute of given name exists
    Attribute openAttribute(string const&name) const;
    Attribute getAttribute(string const&name) const
    { return openAttribute(name); }
    
    /// open attribute, read single datum, and close attribute
    /// \throws Exception  if the attribute does not exist, its data space is
    ///                    not scalar, or if the data type is not convertible
    ///                    to the desired type.
    template<typename Tp>
    enable_if_t<is_basic_type_or_string<Tp>()>
    readAttribute(string const&name, Tp &datum) const
    { openAttribute(name).read(datum); }

    /// open attribute, read 1D data vector, and close attribute
    /// \throws Exception  if the attribute does not exist, its data space is
    ///                    not 1D, or if the data type is not convertible to
    ///                    the desired type.
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    readAttribute(string const&name, Container &container) const
    { openAttribute(name).read(container); }
        
    /// create an attribute with given name, data space, and data type
    Attribute createAttribute(string const&name,
	DataSpace const&space, DataType const&dtype);

    /// create an attribute with basic type
    template<typename Tp>
    Attribute createAttribute(string const&name, DataSpace space)
    { return createAttribute(name,space,DataType::For<Tp>()); }

    /// create attribute, write basic type or string, and close an attribute  
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    writeAttribute(string const&name, Tp const&datum);
    void writeAttribute(string const&name, const char*strg);
    void writeAttribute(string const&name, string const&strg)
    { writeAttribute(name,strg.c_str()); }

    /// create attribute, write 1D data vector, and close an Attribute
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    writeAttribute(string const&name, Container const&container);

    /// update an existing or write a new attribute with scalar data
    /// \throws Exception if data type or space mismatches that of existing
    ///                   attribute
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    updateAttribute(string const&name, Tp const&datum);
    void updateAttribute(string const&name, const char*strg);
    void updateAttribute(string const&name, string const&strg)
    { updateAttribute(name,strg.c_str()); }

    /// update an existing or write a new attribute with 1D data
    /// \throws Exception  if datat ype or space mismatches that of existing
    ///                    attribute or in case of size mismatch
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    updateAttribute(string const&name, Container const&container);

  protected:
    string local_name, full_name;
    Location(id_type id, const Location*parent, string const&name);
    friend struct File;
};

///
/// a named data set with attributes
///
/// \todo  high-level support for multi-dimensional I/O
struct DataSet
  : Location
{
    /// data space of this data set on disc/file
    DataSpace space() const;
    DataSpace getDataSpace() const
    { return space(); }

    /// data type of this data set on disc/file
    DataType type() const;
    DataType dataType() const
    { return type(); }
    DataType getDataType() const
    { return dataType(); }

    /// the create properties used at creation
    DataSetCreateProperties createProperties() const;
    DataSetCreateProperties getCreateProperties() const
    { return createProperties(); }
    
    /// the access properties used at creation
    DataSetAccessProperties accessProperties() const;
    DataSetAccessProperties getAccessProperties() const
    { return accessProperties(); }

    /// extend extent (but not its rank)
    /// \param[in] newExtent   dimensions beyond rank are ignored and missing
    ///                        dimensions replaced by current extent.
    /// \param[in] warnReduce  if true and newExtent[]< current extent, issue a
    ///                        warning
    /// \throws Exception      if newExtent extends beyond maximum extent or
    ///                        if we have no write access
    void extend(size_table const&newExtent, bool warnReduce=true)
    { extend(newExtent.size(), newExtent.data(), warnReduce); }
    void extend(size_type newExtentInDim0, bool warnReduce=true)
    { extend(1, &newExtentInDim0, warnReduce); }
    
    /// low-level read interface
    /// \param[in] buffer      buffer to read into, must have enough space
    /// \param[in] memSpace    extent & selection for writing into buffer
    /// \param[in] memType     type of data to write into buffer
    /// \param[in] fileSelect  selection for reading from file (all if omitted)
    /// \throws Exception  if memType is a string type, if the amount of data
    ///                    selected on file does not match with those in
    ///                    memory, or if the data type in memory cannot be
    ///                    converted to that on file
    /// \note for strings use read(string*) below.
    void read(void*buffer, DataSpace const&memSpace, DataType const&memType,
	DataSpace::Selection const&fileSelect) const;
    void read(void*buffer, DataSpace const&memSpace, DataType const&memType)
      const
    { read(buffer,memSpace,memType,DataSpace::Selection{}); }

    /// read selected data of basic type or string
    /// \tparam    Tp          basic type or string
    /// \param[in] buffer      buffer to transfer from, must have enough data
    /// \param[in] memSpace    extent of & selection for buffer
    /// \param[in] fileSelect  selection for reading from file (all if omitted)
    /// \note  strings read will have the encoding (ASCII or UTF-8) as on file
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    read(Tp*buffer, DataSpace const&memSpace,
	DataSpace::Selection const&fileSelect) const
    { read(buffer, memSpace, DataType::For<Tp>(), fileSelect); }
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    read(Tp*buffer, DataSpace const&memSpace) const
    { read(buffer, memSpace, DataType::For<Tp>()); }

    void read(string*buffer, DataSpace const&memSpace,
	DataSpace::Selection const&fileSelect) const;
    void read(string*buffer, DataSpace const&memSpace) const
    { read(buffer,memSpace,DataSpace::Selection{}); }

    /// read all data of basic type or string into resizeable 1D container
    /// \tparam    Container   container type with resize() and data()
    /// \param[in] container   data buffer to read into (will be resized)
    /// \param[in] fileSelect  selection for reading from file (all if omitted)
    template<typename Container>
    enable_if_t<is_resizeable_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>()>
    read(Container &container, DataSpace::Selection const&fileSelect) const;

    /// read all data of basic type or string into resizeable 1D container
    /// \tparam    Container   container type with resize() and data()
    /// \param[in] container   data buffer to read into (will be resized)
    template<typename Container>
    enable_if_t<is_resizeable_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>()>
    read(Container &container) const;
    
    /// low-level write interface
    /// \param[in] buffer      buffer to transger from, must have enough data
    /// \param[in] memSpace    extent of & selection for buffer
    /// \param[in] memType     type of data to transfer from buffer
    /// \param[in] fileSelect  selection for writing to file (all if omitted)

    /// \throws Exception  if memType is a string type, if the file is opened
    ///                    w/o write access, if the amount of data selected on
    ///                    file does not match with those in memory space, if
    ///                    the data types on file and in memory cannot be
    ///                    converted, or if some input is invalid/corrupt
    /// \note   for strings use write(const string*) below
    void write(const void*buffer, DataSpace const&memSpace,
	DataType const&memType, DataSpace::Selection const&fileSelect);
    void write(const void*buffer, DataSpace const&memSpace,
	DataType const&memType)
    { write(buffer,memSpace,memType,DataSpace::Selection{}); }

    /// write selected data of basic type or string
    /// \tparam    Tp          basic type or string
    /// \param[in] buffer      buffer to transfer from, must have enough data
    /// \param[in] memSpace    extent of & selection for buffer
    /// \param[in] fileSelect  selection for writing to file
    /// \param[in] utf8        (string output only) use UTF-8 encoding?
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    write(const Tp*buffer, DataSpace const&memSpace,
	DataSpace::Selection const&fileSelect)
    { write(buffer, memSpace, DataType::For<Tp>(), fileSelect); }
    template<typename Tp>
    enable_if_t<is_basic_type<Tp>()>
    write(const Tp*buffer, DataSpace const&memSpace)
    { write(buffer, memSpace, DataType::For<Tp>()); }

    void write(const string*buffer, DataSpace const&memSpace,
	DataSpace::Selection const&fileSelect, bool utf8=false);
    void write(const string*buffer, DataSpace const&memSpace, bool utf8=false)
    { write(buffer,memSpace,DataSpace::Selection{},utf8); }

    /// write all of 1D container of basic type or string to DataSet
    /// \tparam    Container   container type with size() and data()
    /// \param[in] container   data to write
    /// \param[in] fileSelect  selection for writing to file
    /// \param[in] utf8        (string output only) use UTF-8 encoding?
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    write(Container const&container, DataSpace::Selection const&fileSelect)
    { write(container.data(),DataSpace(container.size()),fileSelect); }
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		std::is_same<string,typename Container::value_type>::value>
    write(Container const&container, DataSpace::Selection const&fileSelect,
	bool utf8=false)
    { write(container.data(),DataSpace(container.size()),fileSelect,utf8); }
	
    /// write all of 1D container of basic type or string to all of DataSet
    /// \tparam    Container   container type with size() and data()
    /// \param[in] container   data to write
    /// \param[in] utf8        (string output only) use UTF-8 encoding?
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type<typename Container::value_type>()>
    write(Container const&container)
    { write(container.data(),DataSpace(container.size())); }
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		std::is_same<string,typename Container::value_type>::value>
    write(Container const&container, bool utf8=false)
    { write(container.data(),DataSpace(container.size()),utf8); }
    
  private:
    friend struct Group;
    DataSet(id_type id, const Location*parent, string const&name)
      : Location(id,parent,name) {}
    void extend(size_type, const size_type*, bool);
};

///
/// a node in the hierarchy, can contain groups, data sets, and attributes.
///
struct Group
  : Location
{
    /// is this a file?
    bool isFile() const
    { return local_name.empty(); }
    bool isRoot() const
    { return isFile(); }

    /// 'File' or 'Group'
    string nameOfType() const
    { return isFile()? "File" : "Group"; }
    
    /// \name  interface to sub-groups
    //@{
    /// type used to store sets of names of datasets and groups
    using nameSet = std::unordered_set<string>;
    /// names of all sub-groups existing on file in alphabetical order
    nameSet const&groupNames() const;
    nameSet const&listGroupNames() const
    { return groupNames(); }
    nameSet const&getGroupNames() const
    { return groupNames(); }

    /// number of sub-groups existing on file
    size_type numGroups() const
    { return groupNames().size(); }
    size_type getNumGroups() const
    { return numGroups(); }

    /// does sub-group 'name' exist on file (whether it's open or not)?
    bool hasGroup(string const&name) const
    { return groupNames().count(name) > 0; }

    /// create a new sub-group on file
    Group createGroup(string const&name);

    /// open an existing sub-group
    /// \throws Exception  if group of that name does not exist on file
    Group openGroup(string const&name) const;
    Group getGroup(string const&name) const
    { return openGroup(name); }
    
    //@}
    
    /* BEGIN work in progress
    
    /// create a soft link (via name) to another group or file
    /// \param[in] relative   use relative file name for linking external files
    Group createSoftLink(string const&name, Group const&group,
	bool relative=true);

    /// create a hard link (via address) to another group in the same file
    /// \details a hard link used the target file address
    /// \param[in] relative  use relative file name for linking external files
    Group createHardLink(string const&name, Group const&group);

    END   work in progress */


    /// \name  interface to data sets
    //@{

    /// names of all data sets in alphabetical order
    nameSet const&dataSetNames() const;
    nameSet const&listDataSetNames() const
    { return dataSetNames(); }
    nameSet const&getDataSetNames() const
    { return dataSetNames(); }

    /// number of data sets
    size_type numDataSets() const
    { return dataSetNames().size(); }
    size_type getNumDataSets() const
    { return numDataSets(); }

    /// does a data set with given name exist on file (open or not)?
    bool hasDataSet(string const&name) const
    { return dataSetNames().count(name) > 0; }
    
    /// create a new data set (on file) with given name, data space, data type,
    /// and properties for creation and access
    /// \details  Extendible data sets (as specified by the data space) require
    ///           a chunked layout. If createProps is default constructed,
    ///           suitable chunk sizes are selected, otherwise createProps must
    ///           provide chunk sizes (see struct Chunked).
    DataSet createDataSet(string const&name,
	DataSpace const&space, DataType const&type,
	DataSetCreateProperties const&createProps=DataSetCreateProperties{},
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{});
    DataSet createDataSet(string const&name,
	DataType const&type, DataSpace const&space,
	DataSetCreateProperties const&createProps=DataSetCreateProperties{},
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{})
    { return createDataSet(name,space,type,createProps,accessProps); }    
      
    /// open a data set
    /// \throws Exception  if data set of that name does not exist
    DataSet openDataSet(string const&name,
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{})
      const;
    DataSet getDataSet(string const&name,
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{})
      const
    { return openDataSet(name,accessProps); }

    /// create a new data set suitable to store data, but don't write them
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>(),
		DataSet>
    createDataSetFor(string const&name, Container const&container,
	DataSetCreateProperties const&createProps=DataSetCreateProperties{},
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{});
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>(),
		DataSet>
    createDataSetFor(string const&name, Container const&container,
	DataSetAccessProperties const&accessProps,
	DataSetCreateProperties const&createProps=DataSetCreateProperties{});

    /// create and write a 1D dataset with the contents of a container
    /// \note for an extendible dataset, use createDataSet() and DataSet::write
    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>()>
    writeDataSet(string const&name, Container const&container,
	DataSetCreateProperties const&createProps=DataSetCreateProperties{},
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{});

    template<typename Container>
    enable_if_t<is_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>()>
    writeDataSet(string const&name, Container const&container,
	DataSetAccessProperties const&accessProps,
	DataSetCreateProperties const&createProps=DataSetCreateProperties{})
    { writeDataSet(name,container,createProps,accessProps); }

    /// open and read a 1D dataset into a 1D container
    template<typename Container>
    enable_if_t<is_resizeable_contiguous_container<Container>() &&
		is_basic_type_or_string<typename Container::value_type>()>
    readDataSet(string const&name, Container&container,
	DataSetAccessProperties const&accessProps=DataSetAccessProperties{});
    
    //@}
    
    /// obtain location information of sub-location given its name
    ///
    /// \throws Exception  if no location of that name exists
    Info subLocationInfo(string const&name) const;
    
    /// flush all buffers in this group/file to disc
    ///
    /// \details Flushes internal buffers and then asks the OS to flush the
    ///          system buffers for open files, when the OS is responsible for
    ///          ensuring that the data is actually flushed to disc.
    void flush();
  private:
    friend struct File;
    Group(id_type, const Location*, string const&, bool);
    void reset_file(id_type, string const&, bool, bool);
    struct loc_names { nameSet dst,grp; };
    mutable std::unique_ptr<loc_names> location_names;
    void retrieve_location_names() const;
    string typeName() const { return isFile()? "file" : "group"; }
    id_type create_dataset_extendible(string const&, id_type, 
	id_type, id_type, id_type);
    id_type create_dataset_nonextendible(string const&, id_type, 
	id_type, id_type, id_type);
};  // struct Group

///
/// a root node/group
///
struct File
  : Group
{
    using Group::isFile;
    /// enum determining behaviour at opening a new file
    enum : unsigned {
	/// elementary: access: read-only access
	ReadOnly  = 0x00u,
	/// elementary: access: read & write
	ReadWrite = 0x01u,
	/// elementary: create/open: truncate existing file, if present
	Truncate  = 0x02u,
	/// elementary: create/open: create new file
	CreateNew = 0x04u,
	/// derived:  open existing file for I (input)
	OpenForReading = ReadOnly,
	/// derived:  open existing file for I/O
	OpenForWriting = ReadWrite,
	/// derived:  create new file for I/O, fails if file of same name exist
	CreateForWriting = ReadWrite | CreateNew,
	/// derived:  open file for I/O, possibly deleting old file of same name
	OverWriting    = ReadWrite | Truncate
    };

    /// is the file <file_name> an HDF5 file?
    static bool isHDF5(string const&file_name);
    static bool isFile(string const&file_name)
    { return isHDF5(file_name); }

    /// total file size on disc
    size_type size() const;
    size_type getSize() const
    { return size(); }
    
    /// is the file open?
    bool isOpen() const
    { return isValid(); }

    /// is this file for read-only?
    /// \throws Exception  if !isOpen()
    bool readOnly() const;
    bool isReadOnly() const
    { return readOnly(); }

    /// is this file for writing?
    bool writing() const
    { return isOpen() && !readOnly(); }
    bool isReadWrite() const
    { return writing(); }

    /// default constructor does nothing
    File();

    /// construct from file name and open mode
    ///
    /// \param[in] openFlags  must contain Truncate xor CreateNew
    File(string const&name, unsigned openFlags = OpenForReading,
	FileAccessProperties const&accessProps = FileAccessProperties());

    /// close previous file and open a new file (for reading or writing)
    ///
    /// \details provided for completeness and best avoided (see notes for
    ///          File::close() below)
    void open(string const&name, unsigned openFlags = OpenForReading,
	FileAccessProperties const&accessProps = FileAccessProperties());

    /// close file (flushes all data)
    ///
    /// \details provided for completeness and best avoided (see notes).
    ///
    /// \note    All open objects in this file will be closed, leaving any
    ///          associated remaining h5pp::Objects dangling. Best practice is
    ///          to avoid this by limiting the scope of such h5pp::Objects,
    ///          which is most naturally implemented using RAII, when Files
    ///          are only opened and closed by con- and de-structors, i.e.
    ///          \code
    ///          {
    ///              File file("file_name",open_mode);
    ///              < code working on file "file_name" >
    ///          } // end of scope: all objects are destroyed in reverse order
    ///          \endcode
    ///
    /// \note    A file is always closed at destruction. The difference is only
    ///          that close() may throw an exception, while h5pp never throws
    ///          form a destructor, but reports errors to stderr.
    void close(bool nothrow=false)
    { reset_file(Object::invalid_id, "", false, nothrow); }

    /// obtain the file name (empty string if !isOpen())
    string name() const { return fileName(); }
    string getName() const { return name(); }

    /// get file access properties used at creation/opening
    FileAccessProperties accessProperties() const;
    FileAccessProperties getAccessProperties() const
    { return accessProperties(); }

  private:
    static id_type open(string const&, unsigned, id_type);
    static bool create_file(unsigned openFlags) noexcept
    { return openFlags & (Truncate | CreateNew); }
};  // struct File

} // namespace h5pp

#include "bits/h5pp_impl.hpp"

#ifdef __clang__
#  pragma clang diagnostic pop
#endif

#endif // h5pp_hpp
